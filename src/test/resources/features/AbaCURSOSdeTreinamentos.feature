#language: pt

Funcionalidade: Aba-CURSOS parametro-TREINAMENTOS.

#CURSO-PDF
@Done03
@Done
  Esquema do Cenário:[Cenario 01 teste conteudo VISUALIZAR NO CELULAR | PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento PDF para baixo
    E clico no curso dentro da aba CURSOS em PDF
    E clico no curso no documento PDF
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 02 teste conteudo que NÃO BAIXAR NO CELULAR | PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento SCORM para baixo
    E clico no menu no canto superior direito do documento PDF
    E clico na opcao BAIXAR NO CELULAR no menu do documento PDF
    E clico em OK dentro da mensagem Nao foi possivel realizar esta acao
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 03 teste conteudo COLOCAR NA LISTA | PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina de CURSOS para baixo
    E clico no menu no canto superior direito do documento PDF
    E clico na opcao COLOCAR NA LISTA no menu do documento PDF
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 04 teste conteudo REMOVER DA LISTA | PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento PDF para baixo
    E clico no menu no canto superior direito do documento PDF
    E clico na opcao RETIRAR DA LISTA no menu do documento PDF
    E clico OK dentro da mensagem O curso foi removido da sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 05 teste conteudo SATISFACAO GERAL | PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento PDF para baixo
    E visualizo e clico no curso PDF
    E clico para executar o curso PDF
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 06 teste conteudo que AVALIAR CURSO APOS CONCLUIR | PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento PDF para baixo
    E clico no menu no canto superior direito do documento PDF
    E clico na opcao AVALIAR CURSO no menu do documento PDF
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#CURSO-LINK EXTERNO
@Done03
@Done
  Esquema do Cenário:[Cenario 07 teste conteudo VISUALIZAR NO CELULAR | LINK EXTERNO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento LINK para baixo
    E clico no menu no canto superior direito do documento LINK
    E clico no curso do documento LINK
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 08 teste conteudo COLOCAR NA LISTA | LINK EXTERNO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento LINK para baixo
    E clico no menu no canto superior direito do documento LINK
    E clico na opcao COLOCAR NA LISTA no menu do documento LINK
    E visualizo a mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 09 teste conteudo REMOVER DA LISTA | LINK EXTERNO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento LINK para baixo
    E clico no menu no canto superior direito do documento LINK
    E clico na opcao COLOCAR NA LISTA no menu do documento LINK
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na opcao RETIRAR DA LISTA no menu do documento LINK
    E clico OK dentro da mensagem O curso foi removido da sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 10 teste conteudo SATISFACAO GERAL | LINK EXTERNO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento LINK para baixo
    E visualizo e clico no curso LINK
    E clico para executar o curso LINK
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 11 teste conteudo que AVALIAR CURSO APOS CONCLUIR | LINK EXTERNO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento LINK para baixo
    E clico no menu no canto superior direito do documento LINK
    E clico na opcao AVALIAR CURSO no menu do documento LINK
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#CURSO-SCORM
@Done03
@Done
  Esquema do Cenário:[Cenario 12 teste conteudo VISUALIZAR NO CELULAR | SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento SCORM para baixo
    E clico no curso dentro da aba CURSOS em SCORM
    E clico no Argumentar dentro do documento SCORM
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 13 teste conteudo que NÃO BAIXAR NO CELULAR | SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento SCORM para baixo
    E clico no menu no canto superior direito do documento SCORM
    E clico na opcao BAIXAR NO CELULAR no menu do documento SCORM
    E clico em OK dentro da mensagem Nao foi possivel realizar esta acao
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 14 teste conteudo COLOCAR NA LISTA | SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento SCORM para baixo
    E clico no menu no canto superior direito do documento SCORM
    E clico na opcao COLOCAR NA LISTA no menu do documento SCORM
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 15 teste conteudo REMOVER DA LISTA | SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento SCORM para baixo
    E clico no menu no canto superior direito do documento SCORM
    E clico na opcao COLOCAR NA LISTA no menu do documento SCORM
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na opcao RETIRAR DA LISTA no menu do documento SCORM
    E clico OK dentro da mensagem O curso foi removido da sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 16 teste conteudo SATISFACAO GERAL | SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento SCORM para baixo
    E visualizo e clico no curso SCORM
    E clico para executar o curso SCORM
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 17 teste conteudo que AVALIAR CURSO APOS CONCLUIR | SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento SCORM para baixo
    E clico no menu no canto superior direito do documento SCORM
    E clico na opcao AVALIAR CURSO no menu do documento SCORM
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#CURSO-VIDEO
@Done03
@Done
  Esquema do Cenário:[Cenario 18 teste conteudo VISUALIZAR NO CELULAR | VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento VIDEO para baixo
    E clico no menu no canto superior direito do documento VIDEO
    E clico no curso do documento VIDEO
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 19 teste conteudo BAIXAR NO CELULAR | VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina para baixo em CURSOS
    E visualizo e clico no menu tres pontos lado direito dentro do CURSO
    E clico na opcao BAIXAR NO CELULAR no menu do video
    E aparece mensagem Este curso ira ocupar MB deseja continuar?
    E clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar?
    E aguardo o download ser concluido
    E visualizo e clico no menu tres pontos lado direito dentro do CURSO
    E aguardo o download ser concluido e vejo a opcao EXCLUIR DO CELULAR
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 20 teste conteudo BAIXAR NO CELULAR E VOLTAR| VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina para baixo em CURSOS
    E visualizo e clico no menu tres pontos lado direito dentro do CURSO
    E clico na opcao BAIXAR NO CELULAR no menu do video
    E aparece mensagem Este curso ira ocupar MB deseja continuar?
    E clico em VOLTAR dentro da mensagem Este curso ira ocupar MB deseja continuar?
    E visualizo e clico no menu tres pontos lado direito dentro do CURSO
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 21 teste conteudo COLOCAR NA LISTA | VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E visualizo e clico no menu tres pontos lado direito dentro do CURSO
    E clico na opcao COLOCAR NA LISTA no menu do video
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 22 teste conteudo REMOVER DA LISTA | VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E visualizo e clico no menu tres pontos lado direito dentro do CURSO
    E clico no curso dentro da aba CURSOS
    E clico na opcao COLOCAR NA LISTA no menu do video
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na opcao RETIRAR DA LISTA no menu do video
    E clico OK dentro da mensagem O curso foi removido da sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 23 teste conteudo EXCLUIR DO CELULAR CLICO SIM | VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E visualizo e clico no menu tres pontos lado direito dentro do CURSO
    E clico no curso dentro da aba CURSOS
    E clico na opcao BAIXAR NO CELULAR no menu do video
    E clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar?
    E clico na opcao EXCLUIR DO CELULAR
    E clico SIM dentro da mensagem Deseja excluir curso do seu aparelho?
    E Visualizo e clico em OK na mensagem Curso excluido com sucesso!
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 24 teste conteudo EXCLUIR DO CELULAR CLICO NAO | VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E visualizo e clico no menu tres pontos lado direito dentro do CURSO
    E clico no curso dentro da aba CURSOS
    E clico na opcao BAIXAR NO CELULAR no menu do video
    E clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar?
    E clico na opcao EXCLUIR DO CELULAR
    E clico NAO dentro da mensagem Deseja excluir curso do seu aparelho?
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 25 teste conteudo SATISFACAO GERAL | VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E assisto o primeiro video ate o final para avaliacao na aba CURSOS
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 26 teste conteudo que AVALIAR CURSO APOS CONCLUIR | VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento VIDEO para baixo
    E clico no menu no canto superior direito do documento VIDEO
    E clico na opcao AVALIAR CURSO no menu do documento VIDEO
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

#CURSO-HTML
@Done03
@Done
  Esquema do Cenário:[Cenario 27 teste conteudo VISUALIZAR NO CELULAR | HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento HTML para baixo
    E clico no menu no canto superior direito do documento HTML
    E clico no curso do documento HTML
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 28 teste conteudo que NÃO BAIXAR NO CELULAR | HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento HTML para baixo
    E clico no menu no canto superior direito do documento HTML
    E clico na opcao BAIXAR NO CELULAR no menu do documento HTML
    E clico em OK dentro da mensagem Nao foi possivel realizar esta acao
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 29 teste conteudo COLOCAR NA LISTA | HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E clico no menu no canto superior direito do documento HTML
    E clico na opcao COLOCAR NA LISTA no menu do documento HTML
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 30 teste conteudo REMOVER DA LISTA | HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E clico no menu no canto superior direito do documento HTML
    E clico na opcao COLOCAR NA LISTA no menu do documento HTML
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na opcao RETIRAR DA LISTA no menu do HTML
    E clico OK dentro da mensagem O curso foi removido da sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 31 teste conteudo SATISFACAO GERAL | HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E visualizo e clico no curso HTML
    E clico para executar o curso HTML
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 32 teste conteudo que AVALIAR CURSO APOS CONCLUIR | HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento HTML para baixo
    E clico no menu no canto superior direito do documento HTML
    E clico na opcao AVALIAR CURSO no menu do documento HTML
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#CURSO-DOWNLOAD
@Done03
@Done
  Esquema do Cenário:[Cenario 33 teste conteudo VISUALIZAR NO CELULAR | DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento DOWNLOAD para baixo
    E clico no curso dentro da aba CURSOS em DOWNLOAD
    E clico no curso no documento DOWNLOAD
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 34 teste conteudo que NÃO BAIXAR NO CELULAR | DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento DOWNLOAD para baixo
    E clico no menu no canto superior direito do documento DOWNLOAD
    E clico na opcao BAIXAR NO CELULAR no menu do documento DOWNLOAD
    E clico em OK dentro da mensagem Nao foi possivel realizar esta acao
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 35 teste conteudo COLOCAR NA LISTA | DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E clico no menu no canto superior direito do documento DOWNLOAD
    E visualizo a opcao BAIXAR NO CELULAR no menu do documento DOWNLOAD
    E visualizo a opcao COLOCAR NA LISTA no menu do documento DOWNLOAD
    E clico na opcao COLOCAR NA LISTA no menu do documento DOWNLOAD
    E visualizo a mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 36 teste conteudo REMOVER DA LISTA | DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E clico no menu no canto superior direito do documento DOWNLOAD
    E visualizo a opcao BAIXAR NO CELULAR no menu do documento DOWNLOAD
    E visualizo a opcao COLOCAR NA LISTA no menu do documento DOWNLOAD
    E clico na opcao COLOCAR NA LISTA no menu do documento DOWNLOAD
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na opcao RETIRAR DA LISTA no menu do video
    E clico OK dentro da mensagem O curso foi removido da sua lista de favoritos com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 37 teste conteudo SATISFACAO GERAL | DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E visualizo e clico no curso DOWNLOAD
    E clico para executar o curso DOWNLOAD
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 38 teste conteudo que AVALIAR CURSO APOS CONCLUIR | DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E rolo a pagina do documento DOWNLOAD para baixo
    E clico no menu no canto superior direito do documento DOWNLOAD
    E clico na opcao AVALIAR CURSO no menu do documento DOWNLOAD
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#CURSO-LINK INTERATIVO
@Done03
@Done
  Esquema do Cenário:[Cenario 39 teste conteudo VISUALIZAR NO CELULAR | LINK INTERATIVO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento LINK INTERATIVO para baixo
    E clico no curso dentro da aba RECOMENDADOS em LINK INTERATIVO
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 40 teste conteudo COLOCAR NA LISTA | LINK INTERATIVO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento LINK INTERATIVO para baixo
    E clico no menu no canto superior direito do documento LINK INTERATIVO
    E clico na opcao COLOCAR NA LISTA no menu do documento LINK INTERATIVO
    E visualizo a mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 41 teste conteudo REMOVER DA LISTA | LINK INTERATIVO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento LINK INTERATIVO para baixo
    E clico no menu no canto superior direito do documento LINK INTERATIVO
    E clico na opcao COLOCAR NA LISTA no menu do documento LINK INTERATIVO
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na opcao RETIRAR DA LISTA no menu do documento LINK INTERATIVO
    E clico OK dentro da mensagem O curso foi removido da sua lista de favoritos com sucesso
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 42 teste conteudo SATISFACAO GERAL | LINK INTERATIVO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento LINK INTERATIVO para baixo
    E clico no curso dentro da aba RECOMENDADOS em LINK INTERATIVO
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 43 teste conteudo que AVALIAR CURSO APOS CONCLUIR | LINK INTERATIVO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento LINK INTERATIVO para baixo
    E clico no menu no canto superior direito do documento LINK INTERATIVO
    E clico na opcao AVALIAR CURSO no menu do documento LINK INTERATIVO
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

#CURSO-WEBNAR
@Done03
@Done
  Esquema do Cenário:[Cenario 44 teste conteudo VISUALIZAR NO CELULAR | WEBNAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento WEBNAR para baixo
    E clico no curso dentro da aba RECOMENDADOS em WEBNAR
    E clico no curso no documento WEBNAR
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 45 teste conteudo que NÃO BAIXAR NO CELULAR | WEBNAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento WEBNAR para baixo
    E clico no menu no canto superior direito do documento WEBNAR
    E clico na opcao BAIXAR NO CELULAR no menu do documento WEBNAR
    E clico em OK dentro da mensagem Nao foi possivel realizar esta acao
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 46 teste conteudo COLOCAR NA LISTA | WEBNAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento WEBNAR para baixo
    E clico no menu no canto superior direito do documento WEBNAR
    E clico na opcao COLOCAR NA LISTA no menu do documento WEBNAR
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 47 teste conteudo REMOVER DA LISTA | WEBNAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento WEBNAR para baixo
    E clico no menu no canto superior direito do documento WEBNAR
    E clico na opcao RETIRAR DA LISTA no menu do documento WEBNAR
    E clico OK dentro da mensagem O curso foi removido da sua lista de favoritos com sucesso
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 48 teste conteudo SATISFACAO GERAL | WEBNAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento WEBNAR para baixo
    E clico no curso dentro da aba RECOMENDADOS em WEBNAR
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |

@Done03
@Done
  Esquema do Cenário:[Cenario 49 teste conteudo que AVALIAR CURSO APOS CONCLUIR | WEBNAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina do documento WEBNAR para baixo
    E clico no menu no canto superior direito do documento WEBNAR
    E clico na opcao AVALIAR CURSO no menu do documento WEBNAR
    Exemplos:
      | Usuario  |
      | 49293230 |
      | 78406951 |
      | 82068267 |
      | 93529503 |
