#language: pt

Funcionalidade: Aba-TRILHAS parametro-TREINAMENTOS.

@Done05
@Done
  Esquema do Cenário: [Cenario 01 curso Trilha Maquiagem e avaliacao]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico no curso Maquiagem dentro da aba TRILHAS percentual
    E clico fechar msg
    E rolo a pagina para baixo dentro de Trilhas
    E clico no primeiro curso em maquiagem
    E em maquiagem clico para reproduzir o video dentro da Trilha Automacao Teste na aba TRILHAS
    E adianto o video para avaliacao
    E apos concluir video em iniciado vejo a tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas um de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em PROXIMO dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas dois de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em PROXIMO dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas tres de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em CONCLUIR dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado clico em CONFIRMAR dentro da mensagem Avaliacao realizada com sucesso em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done05
@Done
  Esquema do Cenário: [Cenario 02 curso Trilha Perfumaria e avaliacao]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico no curso Perfumaria dentro da aba TRILHAS
    E clico fechar msg
    E rolo a pagina para baixo dentro de Trilhas
    E clico no primeiro curso em maquiagem
    E em Perfumaria clico para reproduzir o video dentro na aba TRILHAS
    E adianto o video para avaliacao
    E apos concluir video em iniciado vejo a tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas um de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em PROXIMO dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas dois de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em PROXIMO dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas tres de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em CONCLUIR dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado clico em CONFIRMAR dentro da mensagem Avaliacao realizada com sucesso em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done05
@Done
  Esquema do Cenário: [Cenario 03 teste conteudo iniciado-concluido-trancado]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTOS
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E visualizo curso concluido dentro da Trilha Automacao Teste na aba TRILHAS
    E visualizo curso iniciado dentro da Trilha Automacao Teste na aba TRILHAS
    E visualizo curso trancado dentro da Trilha Automacao Teste na aba TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  @Done05
  @Done
  Esquema do Cenário: [Cenario 04 teste conteudo-iniciado BAIXAR NO CELULAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E clico no curso iniciado dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico na opcao BAIXAR NO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar? em TRILHAS
    E em iniciado vejo nova tela com opcao EXCLUIR DO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  @Done05
  @Done
  Esquema do Cenário: [Cenario 05 teste conteudo-iniciado BAIXAR NO CELULAR|CANCELAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E clico no curso iniciado dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico na opcao BAIXAR NO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em CANCELAR dentro da mensagem Este curso ira ocupar MB deseja continuar? em TRILHAS
    E em iniciado volto para tela onde vejo as opcoes BAIXAR NO CELULAR e COLOCAR NA LISTA em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  @Done05
  @Done
  Esquema do Cenário: [Cenario 06 teste conteudo-iniciado EXCLUIR DO CELULAR|clico SIM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E clico no curso iniciado dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico na opcao BAIXAR NO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar? em TRILHAS
    E em iniciado clico em EXCLUIR DO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico SIM dentro da mensagem Deseja excluir curso do seu aparelho? em TRILHAS
    E em iniciado visualizo e clico em OK na mensagem Curso excluido com sucesso! em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  @Done05
  @Done
  Esquema do Cenário: [Cenario 07 teste conteudo-iniciado EXCLUIR DO CELULAR|clico NAO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E clico no curso iniciado dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico na opcao BAIXAR NO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar? em TRILHAS
    E em iniciado clico em EXCLUIR DO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico NAO dentro da mensagem Deseja excluir curso do seu aparelho? em TRILHAS
    E em iniciado volto para tela onde vejo as opcoes EXCLUIR DO CELULAR e COLOCAR NA LISTA em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  @Done05
  @Done
  Esquema do Cenário: [Cenario 08 teste conteudo-concluido BAIXAR NO CELULAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E clico no curso concluido dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico na opcao BAIXAR NO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar? em TRILHAS
    E em concluido vejo nova tela com opcao EXCLUIR DO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  @Done05
  @Done
  Esquema do Cenário: [Cenario 09 teste conteudo-concluido BAIXAR NO CELULAR|CANCELAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E clico no curso concluido dentro da Trilha Automacao Teste na aba TRILHAS
    E clico no curso concluido dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico na opcao BAIXAR NO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico em CANCELAR dentro da mensagem Este curso ira ocupar MB deseja continuar? em TRILHAS
    E em concluido volto para tela onde vejo as opcoes BAIXAR NO CELULAR e COLOCAR NA LISTA em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  @Done05
  @Done
  Esquema do Cenário: [Cenario 10 teste conteudo-concluido EXCLUIR DO CELULAR|clico SIM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E clico no curso concluido dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico na opcao BAIXAR NO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar? em TRILHAS
    E em concluido clico em EXCLUIR DO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico SIM dentro da mensagem Deseja excluir curso do seu aparelho? em TRILHAS
    E em concluido visualizo e clico em OK na mensagem Curso excluido com sucesso! em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  @Done05
  @Done
  Esquema do Cenário: [Cenario 11 teste conteudo-concluido EXCLUIR DO CELULAR|clico NAO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E clico no curso concluido dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico na opcao BAIXAR NO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar? em TRILHAS
    E em concluido clico em EXCLUIR DO CELULAR dentro da Trilha Automacao Teste na aba TRILHAS
    E em concluido clico NAO dentro da mensagem Deseja excluir curso do seu aparelho? em TRILHAS
    E em concluido volto para tela onde vejo as opcoes EXCLUIR DO CELULAR e COLOCAR NA LISTA em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  @Done05
  @Done
  Esquema do Cenário: [Cenario 12 teste conteudo-trancado]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Curso com pouco progresso dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E Clico no curso trancado dentro da Trilha Automacao Teste na aba TRILHAS
    E nao consigo acessar o curso trancado dentro da Trilha Automacao Teste na aba TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#touth pendente 649
  @Done05
  @Done
  Esquema do Cenário: [Cenario 13 teste em iniciado percentual avanco no curso Trilha Automacao Teste e avaliacao CONFIRMAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E rolo a pagina para baixo dentro de Trilhas
    E visualizo o percentual atual dentro Trilha Automacao Teste na aba TRILHAS
    E clico no curso iniciado dentro da Trilha Automacao Teste na aba TRILHAS percentual
    E clico fechar msg
    E rolo a pagina para baixo dentro de Trilhas
    E clico no curso iniciando em Trilha Automacao Teste
    E em iniciado clico para reproduzir o video dentro da Trilha Automacao Teste na aba TRILHAS
    E adianto o video para avaliacao
    E apos concluir video em iniciado vejo a tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas um de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em PROXIMO dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas dois de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em PROXIMO dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas tres de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em CONCLUIR dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado clico em CONFIRMAR dentro da mensagem Avaliacao realizada com sucesso em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#touth pendente 649
  @Done05
  @Done
  Esquema do Cenário: [Cenario 14 teste em iniciado percentual avanco no curso Trilha Automacao Teste e avaliacao VOLTAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E visualizo e clico Trilha Automacao Teste dentro da aba TRILHAS
    E rolo a pagina para baixo dentro de Trilhas
    E visualizo o percentual atual dentro Trilha Automacao Teste na aba TRILHAS
    E clico no curso iniciado dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico para reproduzir o video dentro da Trilha Automacao Teste na aba TRILHAS
    E adianto o video para avaliacao
    E apos concluir video em iniciado vejo a tela Satisfacao geral com curso realizado em TRILHAS
    E em iniciado vejo as cinco estrelas um de tres e clico nela dentro da Trilha Automacao Teste na aba TRILHAS
    E em iniciado clico em VOLTAR dentro tela Satisfacao geral com curso realizado em TRILHAS
    E em concluido volto para tela onde vejo as opcoes EXCLUIR DO CELULAR e COLOCAR NA LISTA em TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |
