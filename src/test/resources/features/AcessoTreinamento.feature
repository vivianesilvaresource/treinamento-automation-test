#language: pt

Funcionalidade: Acesso o parametro Treinamentos no menu hamburguer validando abas e satisfacao de curso.

@Done02
@Done
  Esquema do Cenário: [Cenario 01 teste validacao de abas dentro do parametro Treinamento]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E vejo a aba RECOMENDADOS dentro do parametro Treinamentos
    E vejo a aba CURSOS dentro do parametro Treinamentos
    E vejo a aba TRILHAS dentro do parametro Treinamentos
    E vejo a aba CATEGORIAS dentro do parametro Treinamentos
    E vejo a aba MINHA LISTA dentro do parametro Treinamentos
    E vejo a aba MIDIATECA dentro do parametro Treinamentos
    E vejo a aba CERTIFICADOS dentro do parametro Treinamentos
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  #ajustar o curso video/touth pendente PDT6
@Done02
@Done
  Esquema do Cenário: [Cenario 02 teste validacao da aba RECOMENDADOS|AVALIAR SATISFACAO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E visualizo e clico no primeiro video sugerido na aba RECOMENDADOS
    E assisto o primeiro video ate o final para avaliacao
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E visualizo a mensagem Avaliacao realizada com sucesso
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  #ajustar o curso video/touth pendente PDT6
@Done02
@Done
  Esquema do Cenário: [Cenario 03 teste validacao da aba CURSOS|AVALIAR SATISFACAO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CURSOS dentro do parametro TREINAMENTOS
    E visualizo e clico no primeiro video sugerido na aba CURSOS
    E clico para reproduzir o video sugerido na aba CURSOS
    E assisto o primeiro video ate o final para avaliacao
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E visualizo a mensagem Avaliacao realizada com sucesso
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

  #ajustar o curso video/touth pendente PDT6
@Done02
@Done
  Esquema do Cenário: [Cenário 04 teste validação da aba TRILHAS|AVALIAR SATISFAÇÃO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico na opcao Automacao Teste dentro da aba TRILHAS
    E assisto o primeiro video ate o final para avaliacao
    E adianto o video para avaliacao
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
    E visualizo curso concluido dentro da aba TRILHAS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done02
@Done
  Esquema do Cenário: [Cenario 05 teste validacao dos componentes LIDER|aba CATEGORIAS]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E vejo a aba TRILHAS dentro do parametro Treinamentos
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E visualizo Desenvolva seu negocio dentro de CATEGORIAS
    E visualizo Desesenvolvimento para a vida dentro de CATEGORIAS
    E visualizo Lider de Negocios dentro de CATEGORIAS
    E visualizo Natura play dentro de CATEGORIAS
    E visualizo Promeiros passos da Consultoria de Beleza dentro de CATEGORIAS
    E visualizo Produtos dentro de CATEGORIAS
  Exemplos:
    | Usuario  |
    | 49293230 |

@Done02
@Done
  @1
  Esquema do Cenário: [Cenario 05.1 teste validacao dos componentes GERENTE N|aba CATEGORIAS]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E vejo a aba TRILHAS dentro do parametro Treinamentos
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E visualizo Comunicacao nao violenta dentro de CATEGORIAS
    E visualizo Desenvolva seu negocio dentro de CATEGORIAS
    E visualizo Desesenvolvimento para a vida dentro de CATEGORIAS
    E visualizo Lider de Negocios dentro de CATEGORIAS
    E visualizo Modulos Comunicacao Nao Violenta
    E visualizo Natura play dentro de CATEGORIAS
    E visualizo Promeiros passos da Consultoria de Beleza dentro de CATEGORIAS
    E visualizo Produtos dentro de CATEGORIAS
    Exemplos:
      | Usuario  |
      | 78406951 |

@Done02
@Done
  Esquema do Cenário: [Cenario 05.2 teste validacao dos componentes GERENTE V|aba CATEGORIAS]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E vejo a aba TRILHAS dentro do parametro Treinamentos
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E visualizo Comunicacao nao violenta dentro de CATEGORIAS
    E visualizo Desenvolva seu negocio dentro de CATEGORIAS
    E visualizo Desesenvolvimento para a vida dentro de CATEGORIAS
    E visualizo Lider de Negocios dentro de CATEGORIAS
    E visualizo Modulos Comunicacao Nao Violenta
    E visualizo Natura play dentro de CATEGORIAS
    E visualizo Promeiros passos da Consultoria de Beleza dentro de CATEGORIAS
    E visualizo Produtos dentro de CATEGORIAS
    Exemplos:
      | Usuario  |
      | 82068267 |

@Done02
@Done
  Esquema do Cenário: [Cenario 05.3 teste validacao dos componentes CONSULTORA|aba CATEGORIAS]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E vejo a aba TRILHAS dentro do parametro Treinamentos
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E visualizo Desenvolva seu negocio dentro de CATEGORIAS
    E visualizo Desesenvolvimento para a vida dentro de CATEGORIAS
    E visualizo Natura play dentro de CATEGORIAS
    E visualizo Promeiros passos da Consultoria de Beleza dentro de CATEGORIAS
    E visualizo Produtos dentro de CATEGORIAS
    Exemplos:
      | Usuario  |
      | 93529503 |

@Done02
@Done
  Esquema do Cenário: [Cenario 06 teste validacao dos componentes|aba MINHA LISTA]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E vejo a aba TRILHAS dentro do parametro Treinamentos
    E vejo a aba CATEGORIAS dentro do parametro Treinamentos
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done02
@Done
  Esquema do Cenário: [Cenario 07 teste validacao dos componentes|aba MIDIATECA]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E vejo a aba CATEGORIAS dentro do parametro Treinamentos
    E vejo a aba MINHA LISTA dentro do parametro Treinamentos
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done02
@Done
  Esquema do Cenário: [Cenario 08 teste validacao dos cursos|aba CERTIFICADOS]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E vejo a aba TRILHAS dentro do parametro Treinamentos
    E vejo a aba CATEGORIAS dentro do parametro Treinamentos
    E vejo a aba MINHA LISTA dentro do parametro Treinamentos
    E vejo a aba MIDIATECA dentro do parametro Treinamentos
    E clico na aba CERTIFICADOS dentro do parametro TREINAMENTO
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |
    
#bug PDT14
@Done02
@Done
  Esquema do Cenário: [Cenario 09 teste validacao campo de Busca]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E visualizo e clico no icone Busca no canto superior direito
    E aparece um campo de busca sinalizando com mensagem Buscar por Cursos
    E clico e insiro "encceja" no campo Buscar por Cursos
    E visualizo e clico no icone Busca no canto superior direito
    E visualizo resultado de busca especifico no campo de Busca
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done02
@Done
  Esquema do Cenário: [Cenario 10 teste validacao busca em Filtrar EM ANDAMENTO clico ACAO FILTRAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E visualizo e clico no icone Filtrar no canto superior direito
    E visualizo e clico Em andamento no Filtrar
    E visualizo e clico no icone acao Filtrar no canto superior direito
    E visualizo e clico no curso em andamento dentro do parametro TREINAMENTOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done02
@Done
  Esquema do Cenário: [Cenario 11 teste validacao busca em Filtrar EM ANDAMENTO clico ACAO Cancelar]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E visualizo e clico no icone Filtrar no canto superior direito
    E visualizo e clico Em andamento no Filtrar
    E visualizo e clico no icone acao Cancelar no canto superior esquerdo
    E retorno a tela inicial de Treinamentos na aplicacao
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done02
@Done
  Esquema do Cenário: [Cenario 12 teste validacao busca em Filtrar Concluido clico ACAO Filtrar]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E visualizo e clico no icone Filtrar no canto superior direito
    E visualizo e clico Concluido no Filtrar
    E visualizo e clico no icone acao Filtrar no canto superior direito
    E visualizo meus cursos Concluido dentro do parametro TREINAMENTOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done02
@Done
  Esquema do Cenário: [Cenario 13 teste validacao busca em Filtrar Concluido clico ACAO Cancelar]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    #E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS E CURSOS dentro do menu hamburguer
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E visualizo e clico no icone Filtrar no canto superior direito
    E visualizo e clico Concluido no Filtrar
    E visualizo e clico no icone acao Cancelar no canto superior esquerdo
    E retorno a tela inicial de Treinamentos na aplicacao
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |


