#language: pt

Funcionalidade: Aba-MIDIATECA parametro-TREINAMENTOS.
#FAZENDO
@Done09
@Done
  Esquema do Cenário: [Cenario 01 validacao componentes em MIDIATECA]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#FAZENDO
@Done09
@Done
  Esquema do Cenário: [Cenario 02 Curso em MIDIATECA | DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |
