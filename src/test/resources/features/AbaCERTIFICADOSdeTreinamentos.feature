#language: pt

Funcionalidade: Aba-CERTIFICADOS parametro-TREINAMENTOS.

@Done08
@Done
  Esquema do Cenário: [Cenario 01 teste validacao de abas dentro do parametro Treinamento]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
    E clico na aba CERTIFICADOS dentro do parametro TREINAMENTO
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done08
@Done
  Esquema do Cenário: [Cenario 02 ABA-CERTIFICADOS | BAIXAR CERTIFICADO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
    E clico na aba CERTIFICADOS dentro do parametro TREINAMENTO
    E clico na opcao BAIXAR CERTIFICADO dentro de CERTIFICADOS
    E visualizo e clico na opcao BAIXAR dentro do CERTIFICADO escolhido
    E aplicação abre browser e clico em Fazer o download
    E verifico o download com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done08
@Done
  Esquema do Cenário: [Cenario 03 ABA-CERTIFICADOS | BAIXAR CERTIFICADO-VOLTAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
    E clico na aba CERTIFICADOS dentro do parametro TREINAMENTO
    E clico na opcao BAIXAR CERTIFICADO dentro de CERTIFICADOS
    E clico no X para voltar a tela de CERTIFICADOS
    E retorno a tela CERTIFICADOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done08
@Done
  Esquema do Cenário: [Cenario 04 ABA-CERTIFICADOS | BAIXAR CERT-ENVIAR POR E-MAIL]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
    E clico na aba CERTIFICADOS dentro do parametro TREINAMENTO
    E clico na opcao BAIXAR CERTIFICADO dentro de CERTIFICADOS
    E clico em ENVIAR POR EMAIL dentro de CERTIFICADOS
    E clico no campo Digite o endereco de email e insiro "email aqui"
    E clico ENVIAR dentro de CERTIFICADOS
    E retorno a tela de opcao BAIXAR CERTIFICADOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done08
@Done
  Esquema do Cenário: [Cenario 05 ABA-CERTIFICADOS | BAIXAR CERT-ENVIAR POR E-MAIL INVALIDO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
    E clico na aba CERTIFICADOS dentro do parametro TREINAMENTO
    E clico na opcao BAIXAR CERTIFICADO dentro de CERTIFICADOS
    E clico em ENVIAR POR EMAIL dentro de CERTIFICADOS
    E clico no campo Digite o endereco de email e insiro "email INVALIDO AQUI aqui"
    E clico ENVIAR dentro de CERTIFICADOS
    E retorno a tela de opcao BAIXAR CERTIFICADOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done08
@Done
  Esquema do Cenário: [Cenario 06 ABA-CERTIFICADOS | CERT-ENVIAR POR E-MAIL NÃO INSERIR E-MAIL]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
    E clico na aba CERTIFICADOS dentro do parametro TREINAMENTO
    E clico na opcao BAIXAR CERTIFICADO dentro de CERTIFICADOS
    E clico em ENVIAR POR EMAIL dentro de CERTIFICADOS
    E clico ENVIAR dentro de CERTIFICADOS
    E visualizo mensagem Informe o email
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done08
@Done
  Esquema do Cenário: [Cenario 07 ABA-CERTIFICADOS | VISUALIZAR PROGRESSO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
    E clico na aba CERTIFICADOS dentro do parametro TREINAMENTO
    E clico em curso nao concluido dentro de CERTIFICADOS
    E clico em Reproduzir curso dentro de CERTIFICADOS
    E clico em PDF para abrir o curso em CERTIFICADOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#FAZENDO
@Done08
@Done
  Esquema do Cenário:[Cenario 08 ABA-CERTIFICADOS | VALIDAR MENU CURSO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba TRILHAS dentro do parametro TREINAMENTO
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico na aba MIDIATECA dentro do parametro TREINAMENTOS
    E clico na aba CERTIFICADOS dentro do parametro TREINAMENTO
    E clico em curso nao concluido dentro de CERTIFICADOS
    E visualizo e clico no menu tres pontos lado direito dentro de CERTIFICADOS
    E clico na opcao BAIXAR NO CELULAR no menu do documento CERTIFICADOS
    E visualizo a mensagem n
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |
