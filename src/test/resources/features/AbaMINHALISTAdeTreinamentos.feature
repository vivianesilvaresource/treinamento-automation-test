#language: pt

Funcionalidade: Aba-MINHA LISTA parametro-TREINAMENTOS.

@Done06
@Done
  Esquema do Cenário:[Cenario 01 teste validacao de aba dentro do parametro Treinamento]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E vejo a aba MINHA LISTA dentro do parametro Treinamentos
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#CURSO VIDEO
@Done06
@Done
  Esquema do Cenário:[Cenario 02 de MINHA LISTA|VIDEO COLOCAR NA LISTA]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina para baixo dentro de RECOMENDADOS
    E clico no menu no canto superior direito do documento VIDEO
    E clico na opcao COLOCAR NA LISTA no menu do video
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento VIDEO
    E visualizo a opcao RETIRAR DA LISTA dentro de RECOMENDADOS
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 03 teste conteudo VISUALIZAR NO CELULAR | VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento VIDEO
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 04 de MINHA LISTA|VIDEO RETIRAR DA LISTA]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento Minha Lista para baixo
    E clico no menu no canto superior direito do documento VIDEO
    E visualizo e clico na opcao RETIRAR DA LISTA dentro de MINHA LISTA
    E visualizo a mensagem Curso excluido com sucesso e clico OK dentro da mensagem em VIDEO MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 05 de MINHA LISTA|VIDEO BAIXAR NO CELULAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido de MINHA LISTA
    E visualizo e clico na opcao BAIXAR NO CELULAR dentro do primeiro video sugerido no MINHA LISTA
    E visualizo a mensagem Este curso irá ocupar MB de seu aparelho, deseja continuar? em VIDEO MINHA LISTA
    E clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar em VIDEO MINHA LISTA
    E visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido em VIDEO MINHA LISTA
    E visualizo a opcao EXCLUIR DO CELULAR dentro do dentro do primeiro video sugerido em VIDEO MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 06 de MINHA LISTA|VIDEO BAIXAR NO CELULAR CANCELAR]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido de MINHA LISTA
    E visualizo e clico na opcao BAIXAR NO CELULAR dentro do primeiro video sugerido no MINHA LISTA
    E visualizo a mensagem Este curso irá ocupar MB de seu aparelho, deseja continuar? em VIDEO MINHA LISTA
    E clico em CANCELAR dentro da mensagem Este curso ira ocupar MB deseja continuar em VIDEO MINHA LISTA
    E visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido EM VIDEO MINHA LISTA
    E visualizo a opcao BAIXAR NO CELULAR dentro do primeiro video sugerido em VIDEO MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 07 de MINHA LISTA|VIDEO EXCLUIR DO CELULAR SIM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido de MINHA LISTA
    E visualizo e clico na opcao BAIXAR NO CELULAR dentro do primeiro video sugerido no MINHA LISTA
    E visualizo a mensagem Este curso irá ocupar MB de seu aparelho, deseja continuar? em VIDEO MINHA LISTA
    E clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar em VIDEO MINHA LISTA
    E visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido em VIDEO MINHA LISTA
    E visualizo e clico na opcao EXCLUIR DO CELULAR dentro do primeiro video sugerido em MINHA LISTA
    E visualizo a mensagem Deseja excluir o curso Nome do curso aqui do seu aparelho? clico SIM na mensagem em VIDEO MINHA LISTA
    E visualizo a mensagem Curso excluido com sucesso e clico OK dentro da mensagem em VIDEO MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 08 de MINHA LISTA|VIDEO EXCLUIR DO CELULAR NAO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido de MINHA LISTA
    E visualizo e clico na opcao BAIXAR NO CELULAR dentro do primeiro video sugerido em MINHA LISTA
    E visualizo a mensagem Este curso irá ocupar MB de seu aparelho, deseja continuar? em MINHA LISTA
    E clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar em MINHA LISTA
    E visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido em MINHA LISTA
    E visualizo e clico na opcao EXCLUIR DO CELULAR dentro do primeiro video sugerido em MINHA LISTA
    E visualizo a mensagem Deseja excluir o curso Nome do curso aqui do seu aparelho? clico NAO na mensagem em MINHA LISTA
    E visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido em MINHA LISTA
    E visualizo a opcao EXCLUIR DO CELULAR dentro do primeiro video sugerido em MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 09 de MINHA LISTA|VIDEO STATUS CONCLUIDO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo video com status de CONCLUIDO dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 10 teste conteudo SATISFACAO GERAL|VIDEO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento VIDEO para baixo
    E visualizo e clico no curso VIDEO
    E clico para executar o curso VIDEO
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#CURSO LINK EXTERNO
@Done06
@Done
  Esquema do Cenário:[Cenario 11 de MINHA LISTA|COLOCAR NA LISTA LINK EXTERNO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina para baixo dentro de RECOMENDADOS
    E clico no menu no canto superior direito do documento LINK EXTERNO
    E clico na opcao COLOCAR NA LISTA no menu do LINK EXTERNO
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento LINK EXTERNO
    E visualizo a opcao RETIRAR DA LISTA dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 12 teste conteudo VISUALIZAR NO CELULAR | LINK EXTERNO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento LINK EXTERNO
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 13 de MINHA LISTA|RETIRAR DA LISTA LINK EXTERNO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento Minha Lista para baixo
    E clico no menu no canto superior direito do documento LINK EXTERNO
    E visualizo e clico na opcao RETIRAR DA LISTA dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 14 de MINHA LISTA|LINK EXTERNO STATUS CONCLUIDO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo e clico componente Desenvolva seu negocio
    E visualizo LINK EXTERNO com status de CONCLUIDO dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 15 teste conteudo SATISFACAO GERAL | LINK EXTERNO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento LINK EXTERNO para baixo
    E visualizo e clico no curso LINK EXTERNO
    E clico para executar o curso LINK EXTERNO
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

#CURSO SCORM
@Done06
@Done
  Esquema do Cenário:[Cenario 16 de MINHA LISTA|COLOCAR NA LISTA SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina para baixo dentro de RECOMENDADOS
    E clico no menu no canto superior direito do documento SCORM
    E clico na opcao COLOCAR NA LISTA no menu do SCORM
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento SCORM
    E visualizo a opcao RETIRAR DA LISTA dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 17 teste conteudo VISUALIZAR NO CELULAR | SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento SCORM
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 18 de MINHA LISTA|RETIRAR DA LISTA SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento Minha Lista para baixo
    E clico no menu no canto superior direito do documento SCORM
    E visualizo e clico na opcao RETIRAR DA LISTA dentro de MINHA LISTA
    E visualizo a mensagem Curso excluido com sucesso e clico OK dentro da mensagem em SCORM MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 19 de MINHA LISTA|SCORM STATUS CONCLUIDO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo SCORM com status de CONCLUIDO dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 20 teste conteudo SATISFACAO GERAL | SCORM]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento SCORM para baixo
    E visualizo e clico no curso SCORM
    E clico para executar o curso SCORM
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

# CURSO DOWNLOAD
@Done06
@Done
  Esquema do Cenário:[Cenario 21 de MINHA LISTA|COLOCAR NA LISTA DOWNLOAD]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina para baixo dentro de RECOMENDADOS
    E clico no menu no canto superior direito do documento DOWNLOAD
    E clico na opcao COLOCAR NA LISTA no menu do DOWNLOAD
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento DOWNLOAD
    E visualizo a opcao RETIRAR DA LISTA dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 22 teste conteudo VISUALIZAR NO CELULAR | DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento DOWNLOAD
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 23 de MINHA LISTA|RETIRAR DA LISTA DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento Minha Lista para baixo
    E clico no menu no canto superior direito do documento DOWNLOAD
    E visualizo e clico na opcao RETIRAR DA LISTA dentro de MINHA LISTA
    E visualizo a mensagem Curso excluido com sucesso e clico OK dentro da mensagem em DOWNLOAD MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 24 de MINHA LISTA|DOWNLOAD STATUS CONCLUIDO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo DOWNLOAD com status de CONCLUIDO dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 25 teste conteudo SATISFACAO GERAL | DOWNLOAD]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento DOWNLOAD para baixo
    E visualizo e clico no curso DOWNLOAD
    E clico para executar o curso DOWNLOAD
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

# CURSO PDF
@Done06
@Done
  Esquema do Cenário:[Cenario 26 de MINHA LISTA|COLOCAR NA LISTA PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina para baixo dentro de RECOMENDADOS
    E clico no menu no canto superior direito do documento PDF
    E clico na opcao COLOCAR NA LISTA no menu do PDF
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento PDF
    E visualizo a opcao RETIRAR DA LISTA dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 27 teste conteudo VISUALIZAR NO CELULAR | PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento PDF
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 28 de MINHA LISTA|RETIRAR DA LISTA PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento Minha Lista para baixo
    E clico no menu no canto superior direito do documento PDF
    E visualizo e clico na opcao RETIRAR DA LISTA dentro de MINHA LISTA
    E visualizo a mensagem Curso excluido com sucesso e clico OK dentro da mensagem em PDF MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 29 de MINHA LISTA|PDF STATUS CONCLUIDO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo PDF com status de CONCLUIDO dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 30 teste conteudo SATISFACAO GERAL | PDF]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento PDF para baixo
    E visualizo e clico no curso PDF
    E clico para executar o curso PDF
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

# CURSO HTML
@Done06
@Done
  Esquema do Cenário:[Cenario 31 de MINHA LISTA|COLOCAR NA LISTA HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba RECOMENDADOS dentro do parametro TREINAMENTO
    E rolo a pagina para baixo dentro de RECOMENDADOS
    E clico no menu no canto superior direito do documento HTML
    E clico na opcao COLOCAR NA LISTA no menu do HTML
    E clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTOS
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento HTML
    E visualizo a opcao RETIRAR DA LISTA dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 32 teste conteudo VISUALIZAR NO CELULAR | HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E clico em ver todos os favoritos
    E rolo a pagina do documento Minha Lista para baixo
    E clico no curso do documento HTML
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 33 de MINHA LISTA|RETIRAR DA LISTA HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento Minha Lista para baixo
    E clico no menu no canto superior direito do documento SCORM
    E visualizo e clico na opcao RETIRAR DA LISTA dentro de MINHA LISTA
    E visualizo a mensagem Curso excluido com sucesso e clico OK dentro da mensagem em HTML MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 34 de MINHA LISTA|HTML STATUS CONCLUIDO]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E visualizo HTML com status de CONCLUIDO dentro de MINHA LISTA
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done06
@Done
  Esquema do Cenário:[Cenario 35 teste conteudo SATISFACAO GERAL | HTML]
    Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
    #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E que acesso o app Consultoria Natura
    #E clico NAO na msg de nova versao
    E clico e insiro "<Usuario>" no campo Nome na tela inicial
    E clico e insiro "senha123" no campo Senha na tela inicial
    Quando clico em Entrar na tela inicial
    E clico em continuar na mensagem Bloqueio de pedidos Natura
    Então acesso aplicacao com sucesso
    E visualizo e clico no menu hamburguer lateral esquerdo superior
    E rolo a pagina para baixo
    E visualizo e clico no TREINAMENTOS dentro do menu hamburguer
    E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
    E clico na aba CATEGORIAS dentro do parametro TREINAMENTO
    E clico na aba MINHA LISTA dentro do parametro TREINAMENTO
    E rolo a pagina do documento HTML para baixo
    E visualizo e clico no curso HTML
    E clico para executar o curso HTML
    E adianto o video para avaliacao
    E visualizo a mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado um de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado dois de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E validar satisfacao geral com o curso realizado tres de tres
    E clico em proximo dentro da mensagem Satisfacao geral com o curso realizado
    E clico em confirmar dentro da mensagem Avaliacao realizada com sucesso
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |