#language: pt

Funcionalidade: Primeiro acesso Login e logout e extensoes.
@Done01
@Done
 Esquema do Cenário: [Cenario 01 teste usuario existente]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  E clico e insiro "<Usuario>" no campo Nome na tela inicial
  E clico e insiro "senha123" no campo Senha na tela inicial
  Quando clico em Entrar na tela inicial
  #E clico em continuar na mensagem Bloqueio de pedidos Natura
  Então acesso aplicacao com sucesso
 Exemplos:
  | Usuario  |
  | 49293230 |
  | 78406951 |
  | 82068267 |
  | 93529503 |

@Done01
@Done
 Esquema do Cenário: [Cenario 02 teste usuario nao existente]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  E clico e insiro "<Usuario>" no campo Nome na tela inicial
  E clico e insiro "senha123" no campo Senha na tela inicial
  Quando clico em Entrar na tela inicial
  Então visualizo a mensagem informando Usuario ou senha invalidos
  E clico OK na mensagem usuario ou senha invalidos
 Exemplos:
  | Usuario  |
  | ABCDEFGH |
  | abcdefgh |
  | 00000001 |
  | .,;~´\#$ |

#Cenário de melhoria
 Esquema do Cenário: [Cenario 03 teste usuário letras maiúscula na senha]
   Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
   #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
   E que acesso o app Consultoria Natura
   #E clico NAO na msg de nova versao
   E clico e insiro "<Usuario>" no campo Nome na tela inicial
   E clico e insiro "SENHA123" no campo Senha na tela inicial
   E vejo a mensagem no campo senha O capslock esta ativo
   Quando clico em Login na tela inicial
   Então visualizo a mensagem informando Usuario ou senha invalidos
   E clico OK na mensagem usuario ou senha invalidos
  Exemplos:
    | Usuario  |
    | 49293230 |
    | 78406951 |
    | 82068267 |
    | 93529503 |

@Done01
@Done
 Esquema do Cenário: [Cenario 04 teste usuario login e logout clico em Sair]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  E clico e insiro "<Usuario>" no campo Nome na tela inicial
  E clico e insiro "senha123" no campo Senha na tela inicial
  Quando clico em Entrar na tela inicial
  #E clico em continuar na mensagem Bloqueio de pedidos Natura
  Então acesso aplicacao com sucesso
  E visualizo e clico no menu hamburguer lateral esquerdo superior
  E rolo a pagina para baixo
  E clico em sair
  E vejo e clico na opcao SAIR dentro da mensagem Atencao
  #E clico NAO na msg de nova versao
  E logout com sucesso
 Exemplos:
  | Usuario  |
  | 49293230 |
  | 78406951 |
  | 82068267 |
  | 93529503 |

@Done01
@Done
  Esquema do Cenário: [Cenario 05 teste usuario login e logout clico em Retornar]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  E clico e insiro "<Usuario>" no campo Nome na tela inicial
  E clico e insiro "senha123" no campo Senha na tela inicial
  Quando clico em Entrar na tela inicial
  #E clico em continuar na mensagem Bloqueio de pedidos Natura
  Então acesso aplicacao com sucesso
  E visualizo e clico no menu hamburguer lateral esquerdo superior
  E rolo a pagina para baixo
  E clico em sair
  E vejo e clico na opcao RETORNAR dentro da mensagem de Atencao
  E retorna a tela inicial com sucesso
 Exemplos:
  | Usuario  |
  | 49293230 |
  | 78406951 |
  | 82068267 |
  | 93529503 |

@Done01
@Done
  Cenário: [Cenario 06 teste sem preencher campo usuario]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  E clico e insiro "senha123" no campo Senha na tela inicial
  Quando clico em Entrar na tela inicial
  Então visualizo a mensagem informando Usuario ou senha invalidos
  E clico OK na mensagem usuario ou senha invalidos


@Done01
@Done
  Esquema do Cenário: [Cenario 07 teste sem preencher campo senha]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  E clico e insiro "<Usuario>" no campo Nome na tela inicial
  Quando clico em Entrar na tela inicial
  Então visualizo a mensagem informando Usuario ou senha invalidos
  E clico OK na mensagem usuario ou senha invalidos
  Exemplos:
    | Usuario  |
    | 49293230 |

#bug
@Done01
@Done
 Esquema do Cenário: [Cenario 08 teste salvar senha]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  E clico e insiro "<Usuario>" no campo Nome na tela inicial
  E clico e insiro "senha123" no campo Senha na tela inicial
  E clico em Salvar senha
  Quando clico em Entrar na tela inicial
  #E clico em continuar na mensagem Bloqueio de pedidos Natura
  E visualizo e clico no menu hamburguer lateral esquerdo superior
  E rolo a pagina para baixo
  E clico em sair
  Então vejo e clico na opcao SAIR dentro da mensagem Atencao
  E logout com sucesso
  E verifico se aplicacao salvou usuario
  E verifico se aplicacao salvou senha
 Exemplos:
  | Usuario  |
  | 49293230 |

#bug
@Done
  Cenário: [Cenario 09 teste de Esqueceu a senha? com CPF valido]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  Quando clico em Esqueceu a senha
  Então aplicacao abre browser com endereco conforme RNLOG-01
  E preencho o campo Digite seu e-mail, codigo natura ou CPF "86848381497"
  E clico em minimizar o teclado
  E clico em Nao sou um robo
  E clico em Continuar

#bug
@Done
  Cenário: [Cenario 10 teste de Esqueceu a senha? com CPF invalido]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #Dado visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  Quando clico em Esqueceu a senha
  Então aplicacao abre browser com endereco conforme RNLOG-01
  E preencho o campo Digite seu e-mail, codigo natura ou CPF "134567887600"
  E clico em minimizar o teclado
  E clico em Nao sou um robo
  E clico em Continuar
  E vejo mensagem no campo o CPF digitado e invalido

#bug
@Done
  Cenário: [Cenario 11 teste de Esqueceu a senha? com e-mail valido]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  Quando clico em Esqueceu a senha
  Então aplicacao abre browser com endereco conforme RNLOG-01
  E preencho o campo Digite seu e-mail, codigo natura ou CPF "legoinchile01@gmail.com"
  E clico em minimizar o teclado
  E clico em Nao sou um robo
  E clico em Continuar
  E sou direcionado a nova tela Ops Cadastro nao localizado conforme RNLOG-02

#bug
@Done
  Cenário: [Cenario 12 teste de Esqueceu a senha? com e-mail invalido]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  Quando clico em Esqueceu a senha
  Então aplicacao abre browser com endereco conforme RNLOG-01
  E preencho o campo Digite seu e-mail, codigo natura ou CPF "teste@teste.com"
  E clico em minimizar o teclado
  E clico em Nao sou um robo
  E clico em Continuar
  E sou direcionado a nova tela Ops Cadastro nao localizado conforme RNLOG-02

#bug
@Done
  Cenário: [Cenario 13 teste de Esqueceu a senha? com codigo natura valido]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  Quando clico em Esqueceu a senha
  Então aplicacao abre browser com endereco conforme RNLOG-01
  E preencho o campo Digite seu e-mail, codigo natura ou CPF "101"
  E clico em minimizar o teclado
  E clico em Nao sou um robo
  E clico em Continuar
  E sou direcionado a nova tela Ops Cadastro nao localizado conforme RNLOG-02

#bug
@Done
  Cenário: [Cenario 14 teste de Esqueceu a senha? com codigo natura invalido]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  Quando clico em Esqueceu a senha
  Então aplicacao abre browser com endereco conforme RNLOG-01
  E preencho o campo Digite seu e-mail, codigo natura ou CPF "123456t"
  E clico em minimizar o teclado
  E clico em Nao sou um robo
  E clico em Continuar
  E sou direcionado a nova tela Ops Cadastro nao localizado conforme RNLOG-02

@Done01
@Done
  Cenário: [Cenario 15 teste em clicar Quero ser uma consultora]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  Quando clico em QUERO SER CONSULTORA NATURA
  Então aplicacao abre browser com endereco conforme RNLOG-03
  E vejo a opcao CLIQUE AQUI PARA SE CADASTRAR
  E sou direcionado a nova tela de cadastro de usuario

#bug
@Done
  Esquema do Cenário:[Cenario 16 teste em clicar visualizar a senha]
  Dado visualizo mensagem Permitir aplicação acessar localizacao deste dispositivo PERMITIR
  #E visualizo mensagem Permitir aplicação acessar arquivos no dispositivo PERMITIR
  E que acesso o app Consultoria Natura
  #E clico NAO na msg de nova versao
  E clico e insiro "<Usuario>" no campo Nome na tela inicial
  E clico e insiro "senha123" no campo Senha na tela inicial
  E clico no icone para visualizar senha
  E visualizo a senha
  Quando clico em Entrar na tela inicial
  E clico em continuar na mensagem Bloqueio de pedidos Natura
  Então acesso aplicacao com sucesso
 Exemplos:
  | Usuario  |
  | 49293230 |
  | 78406951 |
  | 82068267 |
  | 93529503 |