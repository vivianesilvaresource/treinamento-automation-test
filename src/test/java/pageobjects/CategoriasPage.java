package pageobjects;

import org.openqa.selenium.By;

import actions.AndroidActions;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class CategoriasPage {
    AppiumDriver<MobileElement> driver;
    AndroidActions actions;
    AndroidActions a = new AndroidActions();

  //  private By btnApp = By.id("net.natura.cn:id/edtUserLogin");
  //  private By btnNaoExiste = By.xpath("//*[@text='Exemplo que nao existe']");
    private By btnContent = By.xpath("//*[@text='Content']");
   // private By layout = By.id("net.natura.cn:id/mainLayout");
    public static By ClickAbaCategorias = By.xpath("//android.widget.TextView[@text='CATEGORIAS']");
    public static By ClickDevSeuNegocio = By.xpath("//android.widget.TextView[@text='Desenvolva seu negócio']");
    public static By ClickDevParaVida = By.xpath("//android.widget.TextView[@text='Desenvolvimento para a vida']");
    public static By ClickNaturaPlay = By.xpath("//android.widget.TextView[@text='Natura Play']");
    public static By ClickPriPasDaConsulDeBlz = By.xpath("//android.widget.TextView[@text='Primeiros passos da Consultoria de Beleza']");
    public static By ClickProdutos = By.xpath("//android.widget.TextView[@text='Produtos']");
    public static By ClickCompDevParaVida = By.xpath("//android.widget.TextView[@text='Desenvolvimento para a vida']");
    public static By ClickEuComMinhaLider = By.xpath("//android.widget.TextView[@text='Eu com a minha líder']");
    public static By ClickMenuTresPontos = By.id("net.natura.cn:id/menu_btn_video");
    public static By ClickOkMsgNaoFoiPossivel = By.xpath("//android.widget.TextView[@text='OK']");
    public static By ClickOkMsgAddCursoLista = By.xpath("//android.widget.TextView[@text='OK']");
    public static By ClickOkMsgRetirarLista = By.xpath("//android.widget.TextView[@text='OK']");
    public static By ClickCancelarVideo = By.xpath("//android.widget.TextView[@text='CANCELAR']");
    public static By ClickSimMsgExcluir = By.xpath("//android.widget.TextView[@text='SIM']");
    public static By ClickMelhorDaConsulDeBlz = By.xpath("//android.widget.TextView[@text='O melhor da consultoria de beleza']");
    public static By ClickConfirmarMsg = By.xpath("//android.widget.TextView[@text='CONFIRMAR']");
    public static By ClickOkMsgEcluirDoCelular = By.xpath("//android.widget.TextView[@text='OK']");
    public static By ClickNaoMsgExcluir = By.xpath("//android.widget.TextView[@text='NÃO']");
    public static By VisualizoConcluido = By.xpath("//android.widget.TextView[@text='CONCLUÍDO']");
    public static By ClickTreinamentoInterativo = By.xpath("//android.widget.TextView[@text='Treinamento Interativo']");
    public static By ClickOMelhorDaConsulDeBlz = By.xpath("//android.widget.TextView[@text='Tudo sobre seu espaço digital']");

    public CategoriasPage(AppiumDriver<MobileElement> appiumDriver) {
        driver = appiumDriver;
        actions = new AndroidActions();
    }
    public void clickContent() throws Exception {
        actions.androidClick(btnContent);
    }
    public void clickApp() {
    }

    public void visualizoEClicoComponenteDesenvolvaSeuNegocio() throws Exception {
        actions.androidClick(CategoriasPage.ClickDevSeuNegocio);
    }

    public void clicoNaAbaCATEGORIASDentroDoParametroTREINAMENTO() throws Exception {
        actions.androidClick(CategoriasPage.ClickAbaCategorias);
    }

    public void visualizoEClicoEmDesenvolvimentoParaAAVida() throws Exception {
        actions.androidClick(CategoriasPage.ClickDevParaVida);
    }

    public void visualizoEClicoEmLiderDeNegóciosDentroDeCATEGORIAS() throws Exception {
        actions.androidClick(CategoriasPage.ClickDevParaVida);
    }

    public void visualizoEClicoEmNaturaPlayDentroDeCATEGORIAS() throws Exception {
        actions.androidClick(CategoriasPage.ClickNaturaPlay);
    }

    public void visualizoEClicoEmPrimeirosPassosDaConsultoriaDeBelezaDentroDeCATEGORIAS() throws Exception {
        actions.androidClick(CategoriasPage.ClickPriPasDaConsulDeBlz);
    }

    public void visualizoEClcioEmProdutosDentroDeCATEGORIAS() throws Exception {
        actions.androidClick(CategoriasPage.ClickProdutos);
    }

    public void visualizoEuComAMinhaLiderDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Eu com a minha líder",
                "Eu com a minha líder");
    }

    public void visualizoOMelhorDaConsultoriaDeBelezaDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("O melhor da consultoria de beleza"
                ,"O melhor da consultoria de beleza");
    }

    public void visualizoTreinamentoInterativoDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Treinamento Interativo"
                ,"Treinamento Interativo");
    }

    public void visualizoTudoSobreAVendaOnlineDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Tudo sobre seu espaço digital"
                ,"Tudo sobre seu espaço digital");
    }

    public void visualizoTutoriaisDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Tutoriais"
                ,"Tutoriais");
    }

    public void visualizoVidaFinanceiraDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Vida Financeira"
                ,"Vida Financeira");
    }

    public void visualizoVoceNaCapaDaMinhaNaturaDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Você na Capa da Minha Natura"
                ,"Você na Capa da Minha Natura");
    }

    public void visualizoEClicoComponenteDesenvolvimentoParaAVida() throws Exception {
        actions.androidClick(CategoriasPage.ClickCompDevParaVida);
    }

    public void visualizoAlfabetizaçãoDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Alfabetização"
                ,"Alfabetização");
    }

    public void visualizoAutoconhecimentoDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Autoconhecimento"
                ,"Autoconhecimento");
    }

    public void visualizoBemEstarNaInternetDentroDeDesenvolvimentoParaAVida() throws Exception {
        a.scroll(0.3, 0.0);
        actions.assertStrings("Bem-Estar na Internet"
                ,"Bem-Estar na Internet");
    }

    public void visualizoCidadaniaDigitalDentroDeDesenvolvimentoParaAVida() throws Exception {
        a.scroll(0.4, 0.0);
        actions.assertStrings("Cidadania Digital"
                ,"Cidadania Digital");
    }

    public void visualizoCidadaniaDigitalNasEleicoesDentroDeDesenvolvimentoParaAVida() throws Exception {
        a.scroll(0.5, 0.0);
        actions.assertStrings("Cidadania Digital nas Eleições"
                ,"Cidadania Digital nas Eleições");
    }

    public void visualizoComunicacaoParaNegociosDentroDeDesenvolvimentoParaAVida() throws Exception {
        a.scroll(0.5, 0.0);
        actions.assertStrings("Comunicação para Negócios"
                ,"Comunicação para Negócios");
    }

    public void visualizoCulturaDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Cultura"
                ,"Cultura");
    }

    public void visualizoDireitosDaMulherDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Direitos da mulher"
                ,"Direitos da mulher");
    }

    public void visualizoDiversidadeDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Diversidade"
                ,"Diversidade");
    }

    public void visualizoDiversidadeLGBTDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Diversidade LGBT+"
                ,"Diversidade LGBT+");
    }

    public void visualizoDiversidadeRacialDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Diversidade racial",
                "Diversidade racial");
    }

    public void visualizoEducacaoDosFilhosDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Educação dos filhos"
                ,"Educação dos filhos");
    }

    public void visualizoEmpreendedorismoDigitalDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Empreendedorismo Digital"
                ,"Empreendedorismo Digital");
    }

    public void visualizoENCCEJACienciasNaturezaDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("ENCCEJA – Ciências da natureza"
                ,"ENCCEJA – Ciências da natureza");
    }

    public void visualizoENCCEJACienciasHumanasDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("ENCCEJA – Ciências Humanas"
                ,"ENCCEJA – Ciências Humanas");
    }

    public void visualizoENCCEJALinguagensDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("ENCCEJA – Linguagens"
                ,"ENCCEJA – Linguagens");
    }

    public void visualizoMaternidadeEEducacaoDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Maternidade e Educação"
                ,"Maternidade e Educação");
    }

    public void visualizoENCCEJAMatematicaDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("ENCCEJA - Matemática"
                ,"ENCCEJA - Matemática");
    }

    public void visualizoOrganizandoOOrcamentoDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Organizando o Orçamento"
                ,"Organizando o Orçamento");
    }


    public void visualizoSaudeDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Saúde"
                ,"Saúde");
    }

    public void visualizoSegurancaNaInternetDentroDeDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Segurança na Internet"
                ,"Segurança na Internet");
    }

    public void visualizoOComponenteDesenvolvimentoParaAVida() throws Exception {
        actions.assertStrings("Desenvolvimento para a vida"
                ,"Desenvolvimento para a vida");
    }

    public void visualizoEAgoraConsultoraDentroDeNaturaPlay() throws Exception {
        actions.assertStrings("E agora Consultora?"
                ,"E agora Consultora?");
    }

    public void visualizoEncontroNaturaDigitalDentroDeNaturaPlay() throws Exception {
        actions.assertStrings("Encontro Natura Digital"
                ,"Encontro Natura Digital");
    }

    public void visualizoMandandoBemDentroDeNaturaPlay() throws Exception {
        actions.assertStrings("Mandando Bem"
                ,"Mandando Bem");
    }

    public void visualizoMissaoConsultoriaDentroDeNaturaPlay() throws Exception {
        actions.assertStrings("Missão Consultoria"
                ,"Missão Consultoria");
    }

    public void visualizoComponentePrimeirosPassosDaConsultoriaDeBeleza() throws Exception {
        actions.assertStrings("Primeiros passos da Consultoria de Beleza"
                ,"Primeiros passos da Consultoria de Beleza");
    }

    public void visualizoSeuInicioDentroDePrimeirosPassosDaCosultoriaDeBeleza() throws Exception {
        actions.assertStrings("Seu Início"
                ,"Seu Início");
    }

    public void visualizoComponenteProdutosDentroDeCATEGORIAS() throws Exception {
        actions.assertStrings("Produtos"
                ,"Produtos");
    }

    public void visualizoAcademiaDeProdutosDentroDeProdutos() throws Exception {
        actions.assertStrings("Academia de Produtos"
                ,"Academia de Produtos");
    }

    public void visualizoCabelosDentroDeProdutos() throws Exception {
        actions.assertStrings("Cabelos"
                ,"Cabelos");
    }

    public void visualizoConexaoBelezaDentroDeProdutos() throws Exception {
        actions.assertStrings("Conexão Beleza"
                ,"Conexão Beleza");
    }

    public void visualizoMaquiagemDentroDeProdutos() throws Exception {
        actions.assertStrings("Maquiagem"
                ,"Maquiagem");
    }

    public void visualizoCorebeautyDentroDeProdutos() throws Exception {
        actions.assertStrings("Corebeauty"
                ,"Corebeauty");
    }

    public void visualizoRostoDentroDeProdutos() throws Exception {
        actions.assertStrings("Rosto"
                ,"Rosto");
    }

    public void visualizoEClicoComponenteNaturaPlay() throws Exception {
        actions.androidClick(CategoriasPage.ClickNaturaPlay);
    }

    public void visualizoEClicoComponentePrimeirosPassosDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.ClickPriPasDaConsulDeBlz);
    }

    public void visualizoEClicoEmEuComMinhaLiderDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.androidClick(CategoriasPage.ClickEuComMinhaLider);
    }

    public void visualizoOTituloDoCursoEuComAMinhaLider() throws Exception {
        actions.assertStrings("Eu com a minha líder"
                ,"Eu com a minha líder");
    }

    public void visualizoOComponenteDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Desenvolva seu negócio"
                ,"Desenvolva seu negócio");
    }

    public void visualizoOPrimeiroVideoSugeridoNoEuComAMinhaLiderDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Eu com a minha líder | Pesquisa"
                ,"Eu com a minha líder | Pesquisa");
    }

    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDoCursoSugeridoNoDesenvolvaSeuNegocio() throws Exception {
        actions.androidClick(CategoriasPage.ClickMenuTresPontos);
    }

    public void visualizoAMensagemNaoFoiPossivelRealizarEstaAcaoEClicoOKNaMensagemDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.androidClick(CategoriasPage.ClickOkMsgNaoFoiPossivel);
    }

    public void visualizoAMensagemOCursoFoiAdicionadoASuaListaDeFavoritosComSucessoEClicoOK() throws Exception {
        actions.androidClick(CategoriasPage.ClickOkMsgAddCursoLista);
    }

    public void visualizoAMensagemOCursoFoiRemovidoDaSuaListaDeFavoritosComSucessoEClicoEmOK() throws Exception {
        actions.androidClick(CategoriasPage.ClickOkMsgRetirarLista);
    }

    public void visualizoOPrimeiroVideoSugeridoNoOMelhorDaConsultoriaDeBelezaDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("O melhor da consultoria de beleza"
                ,"O melhor da consultoria de beleza");
    }

    public void clicoEmCANCELARDentroDaMensagemEsteCursoIraOcuparMBDesejaContinuarEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.ClickCancelarVideo);
    }

    public void visualizoAMensagemDesejaExcluirOCursoNomeDoCursoAquiDoSeuAparelhoClicoSIMNaMensagemEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.ClickSimMsgExcluir);
    }

    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDeEuComAMinhaLider() throws Exception {
        actions.androidClick(CategoriasPage.ClickMenuTresPontos);
    }

    public void visualizoEClicoEmOMelhorDaConsultoriaDeBelezaDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.androidClick(CategoriasPage.ClickMelhorDaConsulDeBlz);
    }

    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDoPrimeiroVideoSugeridoOMelhorDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.ClickMenuTresPontos);
    }

    public void clicoEmCONFIRMARDentroDaMensagemEsteCursoIraOcuparMBDesejaContinuarEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.ClickConfirmarMsg);
    }

    public void visualizoAMensagemCursoExcluidoComSucessoEClicoOKDentroDaMensagemEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.ClickOkMsgEcluirDoCelular);
    }

    public void visualizoAMensagemDesejaExcluirOCursoNomeDoCursoAquiDoSeuAparelhoClicoNAONaMensagemEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.ClickNaoMsgExcluir);
    }

    public void clicoOKDentroDaMensagemOCursoFoiAdicionadoASuaListaDeFavoritosComSucessoDentroDeOMelhorDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.ClickOkMsgAddCursoLista);
    }

    public void visualizoVideoComStatusDeCONCLUIDODentroDeOMelhorDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.VisualizoConcluido);
    }

    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDeOMelhorDaConsultoriaDeBeleza() throws Exception {
        actions.androidClick(CategoriasPage.ClickMenuTresPontos);
    }

    public void visualizoEClicoTreinamentoInterativoDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.androidClick(CategoriasPage.ClickTreinamentoInterativo);
    }

    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDeTreinamentoInterativo() throws Exception {
        actions.androidClick(CategoriasPage.ClickMenuTresPontos);
    }

    public void clicoOKDentroDaMensagemOCursoFoiAdicionadoASuaListaDeFavoritosComSucessoDentroDeTreinamentoInterativo() throws Exception {
        actions.androidClick(CategoriasPage.ClickOkMsgAddCursoLista);
    }

    public void visualizoVideoComStatusDeCONCLUIDODentroDeTreinamentoInterativo() throws Exception {
        actions.assertStrings("CONCLUÍDO",
                "CONCLUÍDO");
    }

    public void visualizoEClicoEmTudoSobreAVendaOnlineDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.androidClick(CategoriasPage.ClickOMelhorDaConsulDeBlz);
    }

    public void visualizoComunidadeAprendizagemDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Comunidade de Aprendizagem Natura",
                "Comunidade de Aprendizagem Natura");
    }

    public void visualizoPrataDaCasaDentroDeDesenvolvaSeuNegocio() throws Exception {
        actions.assertStrings("Prata da Casa",
                "Prata da Casa");
    }

}
