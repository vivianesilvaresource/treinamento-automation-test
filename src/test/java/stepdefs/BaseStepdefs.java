package stepdefs;

import configuration.TestContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BaseStepdefs {
	
	protected TestContext context = TestContext.getInstance();
	protected AppiumDriver<MobileElement> driver = context.getDriver();
}
