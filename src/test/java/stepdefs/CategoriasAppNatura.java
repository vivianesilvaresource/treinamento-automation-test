package stepdefs;


import io.cucumber.java.pt.E;
import pageobjects.CategoriasPage;

public class CategoriasAppNatura extends BaseStepdefs {

    CategoriasPage Categorias = new CategoriasPage(driver);


    @E("visualizo e clico componente Desenvolva seu negocio")
    public void visualizoEClicoComponenteDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoEClicoComponenteDesenvolvaSeuNegocio();
    }

    @E("clico na aba CATEGORIAS dentro do parametro TREINAMENTO")
    public void clicoNaAbaCATEGORIASDentroDoParametroTREINAMENTO() throws Exception {
        Categorias.clicoNaAbaCATEGORIASDentroDoParametroTREINAMENTO();
    }

    @E("visualizo e clico em Desenvolvimento para a a vida")
    public void visualizoEClicoEmDesenvolvimentoParaAAVida() throws Exception {
        Categorias.visualizoEClicoEmDesenvolvimentoParaAAVida();
    }

    @E("visualizo e clico em Lider de Negócios dentro de CATEGORIAS")
    public void visualizoEClicoEmLiderDeNegóciosDentroDeCATEGORIAS() throws Exception {
        Categorias.visualizoEClicoEmLiderDeNegóciosDentroDeCATEGORIAS();
    }

    @E("visualizo e clico em Natura Play dentro de CATEGORIAS")
    public void visualizoEClicoEmNaturaPlayDentroDeCATEGORIAS() throws Exception {
        Categorias.visualizoEClicoEmNaturaPlayDentroDeCATEGORIAS();
    }

    @E("visualizo e clico em Primeiros passos da Consultoria de Beleza dentro de CATEGORIAS")
    public void visualizoEClicoEmPrimeirosPassosDaConsultoriaDeBelezaDentroDeCATEGORIAS() throws Exception {
        Categorias.visualizoEClicoEmPrimeirosPassosDaConsultoriaDeBelezaDentroDeCATEGORIAS();
    }

    @E("visualizo e clcio em Produtos dentro de CATEGORIAS")
    public void visualizoEClcioEmProdutosDentroDeCATEGORIAS() throws Exception {
        Categorias.visualizoEClcioEmProdutosDentroDeCATEGORIAS();
    }

    @E("visualizo Eu com a minha lider dentro de Desenvolva seu negocio")
    public void visualizoEuComAMinhaLiderDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoEuComAMinhaLiderDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo O melhor da consultoria de beleza dentro de Desenvolva seu negocio")
    public void visualizoOMelhorDaConsultoriaDeBelezaDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoOMelhorDaConsultoriaDeBelezaDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo Treinamento Interativo dentro de Desenvolva seu negocio")
    public void visualizoTreinamentoInterativoDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoTreinamentoInterativoDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo Tudo sobre a venda online dentro de Desenvolva seu negocio")
    public void visualizoTudoSobreAVendaOnlineDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoTudoSobreAVendaOnlineDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo Tutoriais dentro de Desenvolva seu negocio")
    public void visualizoTutoriaisDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoTutoriaisDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo Vida Financeira dentro de Desenvolva seu negocio")
    public void visualizoVidaFinanceiraDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoVidaFinanceiraDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo Voce na Capa da Minha Natura dentro de Desenvolva seu negocio")
    public void visualizoVoceNaCapaDaMinhaNaturaDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoVoceNaCapaDaMinhaNaturaDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo e clico componente Desenvolvimento para a vida")
    public void visualizoEClicoComponenteDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoEClicoComponenteDesenvolvimentoParaAVida();
    }

    @E("visualizo Alfabetização dentro de Desenvolvimento para a vida")
    public void visualizoAlfabetizaçãoDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoAlfabetizaçãoDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Autoconhecimento dentro de Desenvolvimento para a vida")
    public void visualizoAutoconhecimentoDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoAutoconhecimentoDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Bem-Estar na Internet dentro de Desenvolvimento para a vida")
    public void visualizoBemEstarNaInternetDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoBemEstarNaInternetDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Cidadania Digital dentro de Desenvolvimento para a vida")
    public void visualizoCidadaniaDigitalDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoCidadaniaDigitalDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Cidadania Digital nas Eleicoes dentro de Desenvolvimento para a vida")
    public void visualizoCidadaniaDigitalNasEleicoesDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoCidadaniaDigitalNasEleicoesDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Comunicacao para Negocios dentro de Desenvolvimento para a vida")
    public void visualizoComunicacaoParaNegociosDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoComunicacaoParaNegociosDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Cultura dentro de Desenvolvimento para a vida")
    public void visualizoCulturaDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoCulturaDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Direitos da mulher dentro de Desenvolvimento para a vida")
    public void visualizoDireitosDaMulherDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoDireitosDaMulherDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Diversidade dentro de Desenvolvimento para a vida")
    public void visualizoDiversidadeDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoDiversidadeDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Diversidade LGBT dentro de Desenvolvimento para a vida")
    public void visualizoDiversidadeLGBTDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoDiversidadeLGBTDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Diversidade Racial dentro de Desenvolvimento para a vida")
    public void visualizoDiversidadeRacialDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoDiversidadeRacialDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Educacao dos filhos dentro de Desenvolvimento para a vida")
    public void visualizoEducacaoDosFilhosDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoEducacaoDosFilhosDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Empreendedorismo Digital dentro de Desenvolvimento para a vida")
    public void visualizoEmpreendedorismoDigitalDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoEmpreendedorismoDigitalDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo ENCCEJA-Ciencias Natureza dentro de Desenvolvimento para a vida")
    public void visualizoENCCEJACienciasNaturezaDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoENCCEJACienciasNaturezaDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo ENCCEJA-Ciencias Humanas dentro de Desenvolvimento para a vida")
    public void visualizoENCCEJACienciasHumanasDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoENCCEJACienciasHumanasDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo ENCCEJA-Linguagens dentro de Desenvolvimento para a vida")
    public void visualizoENCCEJALinguagensDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoENCCEJALinguagensDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Maternidade e Educacao dentro de Desenvolvimento para a vida")
    public void visualizoMaternidadeEEducacaoDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoMaternidadeEEducacaoDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo ENCCEJA - Matematica dentro de Desenvolvimento para a vida")
    public void visualizoENCCEJAMatematicaDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoENCCEJAMatematicaDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Organizando o Orcamento dentro de Desenvolvimento para a vida")
    public void visualizoOrganizandoOOrcamentoDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoOrganizandoOOrcamentoDentroDeDesenvolvimentoParaAVida();
    }


    @E("visualizo Saude dentro de Desenvolvimento para a vida")
    public void visualizoSaudeDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoSaudeDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo Seguranca na Internet dentro de Desenvolvimento para a vida")
    public void visualizoSegurancaNaInternetDentroDeDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoSegurancaNaInternetDentroDeDesenvolvimentoParaAVida();
    }

    @E("visualizo o componente Desenvolvimento para a vida")
    public void visualizoOComponenteDesenvolvimentoParaAVida() throws Exception {
        Categorias.visualizoOComponenteDesenvolvimentoParaAVida();
    }

    @E("visualizo E agora Consultora dentro de Natura Play")
    public void visualizoEAgoraConsultoraDentroDeNaturaPlay() throws Exception {
        Categorias.visualizoEAgoraConsultoraDentroDeNaturaPlay();
    }

    @E("visualizo Encontro Natura Digital dentro de Natura Play")
    public void visualizoEncontroNaturaDigitalDentroDeNaturaPlay() throws Exception {
        Categorias.visualizoEncontroNaturaDigitalDentroDeNaturaPlay();
    }

    @E("visualizo Mandando Bem dentro de Natura Play")
    public void visualizoMandandoBemDentroDeNaturaPlay() throws Exception {
        Categorias.visualizoMandandoBemDentroDeNaturaPlay();
    }

    @E("visualizo Missao Consultoria dentro de Natura Play")
    public void visualizoMissaoConsultoriaDentroDeNaturaPlay() throws Exception {
        Categorias. visualizoMissaoConsultoriaDentroDeNaturaPlay();
    }

    @E("visualizo componente Primeiros passos da Consultoria de Beleza")
    public void visualizoComponentePrimeirosPassosDaConsultoriaDeBeleza() throws Exception {
        Categorias.visualizoComponentePrimeirosPassosDaConsultoriaDeBeleza();
    }

    @E("visualizo Seu Inicio dentro de Primeiros passos da Cosultoria de Beleza")
    public void visualizoSeuInicioDentroDePrimeirosPassosDaCosultoriaDeBeleza() throws Exception {
        Categorias.visualizoSeuInicioDentroDePrimeirosPassosDaCosultoriaDeBeleza();
    }

    @E("visualizo componente Produtos dentro de CATEGORIAS")
    public void visualizoComponenteProdutosDentroDeCATEGORIAS() throws Exception {
        Categorias.visualizoComponenteProdutosDentroDeCATEGORIAS();
    }

    @E("visualizo Academia de Produtos dentro de Produtos")
    public void visualizoAcademiaDeProdutosDentroDeProdutos() throws Exception {
        Categorias.visualizoAcademiaDeProdutosDentroDeProdutos();
    }

    @E("visualizo Cabelos dentro de Produtos")
    public void visualizoCabelosDentroDeProdutos() throws Exception {
        Categorias.visualizoCabelosDentroDeProdutos();
    }

    @E("visualizo Conexao Beleza dentro de Produtos")
    public void visualizoConexaoBelezaDentroDeProdutos() throws Exception {
        Categorias.visualizoConexaoBelezaDentroDeProdutos();
    }

    @E("visualizo Maquiagem dentro de Produtos")
    public void visualizoMaquiagemDentroDeProdutos() throws Exception {
        Categorias.visualizoMaquiagemDentroDeProdutos();
    }

    @E("visualizo Corebeauty dentro de Produtos")
    public void visualizoCorebeautyDentroDeProdutos() throws Exception {
        Categorias.visualizoCorebeautyDentroDeProdutos();
    }

    @E("visualizo Rosto dentro de Produtos")
    public void visualizoRostoDentroDeProdutos() throws Exception {
        Categorias.visualizoRostoDentroDeProdutos();
    }

    @E("visualizo e clico componente Natura Play")
    public void visualizoEClicoComponenteNaturaPlay() throws Exception {
        Categorias.visualizoEClicoComponenteNaturaPlay();
    }

    @E("visualizo e clico componente Primeiros passos da Consultoria de Beleza")
    public void visualizoEClicoComponentePrimeirosPassosDaConsultoriaDeBeleza() throws Exception {
        Categorias.visualizoEClicoComponentePrimeirosPassosDaConsultoriaDeBeleza();
    }

    @E("visualizo e clico em Eu com minha lider dentro de Desenvolva seu negocio")
    public void visualizoEClicoEmEuComMinhaLiderDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoEClicoEmEuComMinhaLiderDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo o titulo do Curso Eu com a minha lider")
    public void visualizoOTituloDoCursoEuComAMinhaLider() throws Exception {
        Categorias.visualizoOTituloDoCursoEuComAMinhaLider();
    }

    @E("visualizo o componente Desenvolva seu negocio")
    public void visualizoOComponenteDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoOComponenteDesenvolvaSeuNegocio();
    }

    @E("visualizo o primeiro video sugerido no Eu com a minha lider dentro de Desenvolva seu negocio")
    public void visualizoOPrimeiroVideoSugeridoNoEuComAMinhaLiderDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoOPrimeiroVideoSugeridoNoEuComAMinhaLiderDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo e clico no menu tres pontos lado direito dentro do curso sugerido no Desenvolva seu negocio")
    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDoCursoSugeridoNoDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoEClicoNoMenuTresPontosLadoDireitoDentroDoCursoSugeridoNoDesenvolvaSeuNegocio();
    }


    @E("visualizo a mensagem Nao foi possivel realizar esta acao e clico OK na mensagem dentro de Desenvolva seu negocio")
    public void visualizoAMensagemNaoFoiPossivelRealizarEstaAcaoEClicoOKNaMensagemDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoAMensagemNaoFoiPossivelRealizarEstaAcaoEClicoOKNaMensagemDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo a mensagem O curso foi adicionado a sua lista de favoritos com sucesso e clico OK")
    public void visualizoAMensagemOCursoFoiAdicionadoASuaListaDeFavoritosComSucessoEClicoOK() throws Exception {
        Categorias.visualizoAMensagemOCursoFoiAdicionadoASuaListaDeFavoritosComSucessoEClicoOK();
    }

    @E("visualizo a mensagem O curso foi removido da sua lista de favoritos com sucesso e clico em OK")
    public void visualizoAMensagemOCursoFoiRemovidoDaSuaListaDeFavoritosComSucessoEClicoEmOK() throws Exception {
        Categorias.visualizoAMensagemOCursoFoiRemovidoDaSuaListaDeFavoritosComSucessoEClicoEmOK();
    }

    @E("visualizo o primeiro video sugerido no O melhor da consultoria de beleza de  Desenvolva seu negocio")
    public void visualizoOPrimeiroVideoSugeridoNoOMelhorDaConsultoriaDeBelezaDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoOPrimeiroVideoSugeridoNoOMelhorDaConsultoriaDeBelezaDeDesenvolvaSeuNegocio();
    }

    @E("clico em CANCELAR dentro da mensagem Este curso ira ocupar MB deseja continuar em O melhor da consultoria de beleza")
    public void clicoEmCANCELARDentroDaMensagemEsteCursoIraOcuparMBDesejaContinuarEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        Categorias.clicoEmCANCELARDentroDaMensagemEsteCursoIraOcuparMBDesejaContinuarEmOMelhorDaConsultoriaDeBeleza();
    }

    @E("visualizo a mensagem Deseja excluir o curso Nome do curso aqui do seu aparelho clico SIM na mensagem em O melhor da consultoria de beleza")
    public void visualizoAMensagemDesejaExcluirOCursoNomeDoCursoAquiDoSeuAparelhoClicoSIMNaMensagemEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        Categorias.visualizoAMensagemDesejaExcluirOCursoNomeDoCursoAquiDoSeuAparelhoClicoSIMNaMensagemEmOMelhorDaConsultoriaDeBeleza();
    }

    @E("visualizo e clico no menu tres pontos lado direito dentro de Eu com a minha lider")
    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDeEuComAMinhaLider() throws Exception {
        Categorias.visualizoEClicoNoMenuTresPontosLadoDireitoDentroDeEuComAMinhaLider();
    }

    @E("visualizo e clico em O melhor da consultoria de beleza dentro de Desenvolva seu negocio")
    public void visualizoEClicoEmOMelhorDaConsultoriaDeBelezaDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoEClicoEmOMelhorDaConsultoriaDeBelezaDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo e clico no menu tres pontos lado direito dentro do primeiro video sugerido O melhor da consultoria de beleza")
    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDoPrimeiroVideoSugeridoOMelhorDaConsultoriaDeBeleza() throws Exception {
        Categorias.visualizoEClicoNoMenuTresPontosLadoDireitoDentroDoPrimeiroVideoSugeridoOMelhorDaConsultoriaDeBeleza();
    }

    @E("clico em CONFIRMAR dentro da mensagem Este curso ira ocupar MB deseja continuar em O melhor da consultoria de beleza")
    public void clicoEmCONFIRMARDentroDaMensagemEsteCursoIraOcuparMBDesejaContinuarEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        Categorias.clicoEmCONFIRMARDentroDaMensagemEsteCursoIraOcuparMBDesejaContinuarEmOMelhorDaConsultoriaDeBeleza();
    }

    @E("visualizo a mensagem Curso excluido com sucesso e clico OK dentro da mensagem em O melhor da consultoria de beleza")
    public void visualizoAMensagemCursoExcluidoComSucessoEClicoOKDentroDaMensagemEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        Categorias.visualizoAMensagemCursoExcluidoComSucessoEClicoOKDentroDaMensagemEmOMelhorDaConsultoriaDeBeleza();
    }

    @E("visualizo a mensagem Deseja excluir o curso Nome do curso aqui do seu aparelho clico NAO na mensagem em O melhor da consultoria de beleza")
    public void visualizoAMensagemDesejaExcluirOCursoNomeDoCursoAquiDoSeuAparelhoClicoNAONaMensagemEmOMelhorDaConsultoriaDeBeleza() throws Exception {
        Categorias.visualizoAMensagemDesejaExcluirOCursoNomeDoCursoAquiDoSeuAparelhoClicoNAONaMensagemEmOMelhorDaConsultoriaDeBeleza();
    }

    @E("clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso dentro de O melhor da consultoria de beleza")
    public void clicoOKDentroDaMensagemOCursoFoiAdicionadoASuaListaDeFavoritosComSucessoDentroDeOMelhorDaConsultoriaDeBeleza() throws Exception {
        Categorias.clicoOKDentroDaMensagemOCursoFoiAdicionadoASuaListaDeFavoritosComSucessoDentroDeOMelhorDaConsultoriaDeBeleza();
    }

    @E("visualizo video com status de CONCLUIDO dentro de O melhor da consultoria de beleza")
    public void visualizoVideoComStatusDeCONCLUIDODentroDeOMelhorDaConsultoriaDeBeleza() throws Exception {
        Categorias.visualizoVideoComStatusDeCONCLUIDODentroDeOMelhorDaConsultoriaDeBeleza();
    }

    @E("visualizo e clico no menu tres pontos lado direito dentro de O melhor da consultoria de beleza")
    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDeOMelhorDaConsultoriaDeBeleza() throws Exception {
        Categorias.visualizoEClicoNoMenuTresPontosLadoDireitoDentroDeOMelhorDaConsultoriaDeBeleza();
    }

    @E("visualizo e clico Treinamento Interativo dentro de Desenvolva seu negocio")
    public void visualizoEClicoTreinamentoInterativoDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoEClicoTreinamentoInterativoDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo e clico no menu tres pontos lado direito dentro de Treinamento Interativo")
    public void visualizoEClicoNoMenuTresPontosLadoDireitoDentroDeTreinamentoInterativo() throws Exception {
        Categorias.visualizoEClicoNoMenuTresPontosLadoDireitoDentroDeTreinamentoInterativo();
    }

    @E("clico OK dentro da mensagem O curso foi adicionado a sua lista de favoritos com sucesso dentro de Treinamento Interativo")
    public void clicoOKDentroDaMensagemOCursoFoiAdicionadoASuaListaDeFavoritosComSucessoDentroDeTreinamentoInterativo() throws Exception {
        Categorias.clicoOKDentroDaMensagemOCursoFoiAdicionadoASuaListaDeFavoritosComSucessoDentroDeTreinamentoInterativo();
    }

    @E("visualizo video com status de CONCLUIDO dentro de Treinamento Interativo")
    public void visualizoVideoComStatusDeCONCLUIDODentroDeTreinamentoInterativo() throws Exception {
        Categorias.visualizoVideoComStatusDeCONCLUIDODentroDeTreinamentoInterativo();
    }

    @E("visualizo e clico em Tudo sobre a venda online dentro de Desenvolva seu negocio")
    public void visualizoEClicoEmTudoSobreAVendaOnlineDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoEClicoEmTudoSobreAVendaOnlineDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo Comunidade Aprendizagem dentro de Desenvolva seu negocio")
    public void visualizoComunidadeAprendizagemDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoComunidadeAprendizagemDentroDeDesenvolvaSeuNegocio();
    }

    @E("visualizo Prata da Casa dentro de Desenvolva seu negocio")
    public void visualizoPrataDaCasaDentroDeDesenvolvaSeuNegocio() throws Exception {
        Categorias.visualizoPrataDaCasaDentroDeDesenvolvaSeuNegocio();
    }
}