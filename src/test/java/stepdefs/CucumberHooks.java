package stepdefs;

import configuration.TestContext;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import reports.ExtentTestManager;

public class CucumberHooks {
	Scenario scenario;
	
	@Before
	public void setUpTest(Scenario scenario) {
		this.scenario = scenario;
		TestContext.getInstance().startDriver();
		ExtentTestManager.startTest(scenario.getName(), scenario.getId());
	}
	
	@After
	public void afterTest() {
		ExtentTestManager.endTest();
		TestContext.getInstance().endDriver();
	}
}
