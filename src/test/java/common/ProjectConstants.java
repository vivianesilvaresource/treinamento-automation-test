package common;

public class ProjectConstants {

	private static final String FRAMEWORK_CONFIGS_FOLDER=System.getProperty("user.dir")+"/src/test/resources/configs/";
	
	
	public static String getFrameworkConfigsFolder() {
		return FRAMEWORK_CONFIGS_FOLDER;
	}
}
