package common;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;


public class CommonUtils {
	
	
	public static String getProperty(String propriedade, String PathArqProperties) {
		String valorPropriedade=null;
		
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream(PathArqProperties));
			valorPropriedade = prop.getProperty(propriedade);

		}catch (Exception e) {
			e.getMessage();
			
		}
		return valorPropriedade;
			
	}
	
	
	public static String dataAgora() {
		 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");  
		    Date date = new Date();  
		    return (formatter.format(date));  
	}
	
	
	public static String dataAgoraFormato(String formatoDataHora) {
		 SimpleDateFormat formatter = new SimpleDateFormat(formatoDataHora);  
		    Date date = new Date();  
		    return (formatter.format(date));  
	}
	
	
	public static int runRuntimeCommand(String command) throws IOException {
		int returnValue = -1;

		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(command);
		try {
			proc.waitFor();
		} catch (InterruptedException e) {
			e.getMessage();
			Thread.currentThread().interrupt();
			
		}
		returnValue = proc.exitValue();

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

		BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

		// Read the output from the command
		System.out.println("Comando output:\n");
		String s = null;
		while ((s = stdInput.readLine()) != null) {
			System.out.println(s);
		}

		if ((s = stdError.readLine()) != null) {
			// Read any errors from the attempted command
			System.out.println("Erros de comando:\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}
		}
		return returnValue;
	}

	
	public static boolean runRuntimeCommandAndWait(String command, String expectedResult) throws IOException {
		boolean sentencaLocalizada = false;
		
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(command);
		try {
			proc.waitFor();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			e.getMessage();
			
		}
		proc.exitValue();

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

		BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

		// Read the output from the command
		System.out.println("Comando output:\n");
		String s = null;
		while ((s = stdInput.readLine()) != null) {
			System.out.println(s);
		}

		if ((s = stdError.readLine()) != null) {
			// Read any errors from the attempted command
			System.out.println("Erros de comando:\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
				if (s.contains(expectedResult)) {
					System.out.println("Sentença esperada localizada!");
					sentencaLocalizada = true;
					break;
				}
			}
		}
		return sentencaLocalizada;
	}

	
	public static void runRuntimeCommands(List<String>commands) throws IOException {
		
		for (String command : commands) {
			
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(command);
			try {
				proc.waitFor();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				e.getMessage();
				
			}
			System.out.println("Result code para comando: "+command+" -> "+proc.exitValue());
			

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

			// Read the output from the command
			System.out.println("Comando output:\n");
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}

			if ((s = stdError.readLine()) != null) {
				// Read any errors from the attempted command
				System.out.println("Erros de comando:\n");
				while ((s = stdError.readLine()) != null) {
					System.out.println(s);
					
				}
			}
			
		}
		
	}
	
	
}
