package actions;


import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import configuration.TestContext;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import reports.ExtentTestManager;
public class AndroidActions {

	private static final Object TOP = 1;
	private static final Object LAW = 1;
	int timeoutWait = 2;
	protected AppiumDriver<MobileElement> driver = TestContext.getInstance().getDriver();
	protected WebDriverWait wait = new WebDriverWait(driver, timeoutWait);
	TouchAction touch = new TouchAction(driver);
	private static Object MobileDriver;
	private Point StartPoint;
	private String Start;
	private Object EndPoint;
	private int anchorPercentage;
	private int startPercentage;
	private int finalPercentage;


	protected void logPass(String logMessage) {
		ExtentTestManager.getTest().log(LogStatus.PASS, logMessage);
	}

	protected void logFail(String logMessage) {
		ExtentTestManager.getTest().log(LogStatus.FAIL, logMessage);
	}

	protected void logInfo(String logMessage) {
		ExtentTestManager.getTest().log(LogStatus.INFO, logMessage);
	}

	protected void logScreenshot() {
		ExtentTestManager.getTest().log(LogStatus.INFO, ExtentTestManager.getTest().addBase64ScreenShot(
				"data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64)));
	}

	protected void logFaultyCodeLines() {
		ExtentTestManager.getTest().log(LogStatus.FAIL,
				"<b>Linha no caso de teste onde o erro ocorreu:</b> " + Thread.currentThread().getStackTrace()[4].toString()
						+ "<br/> <b>Linha no Page Object onde o erro ocorreu:</b> "
						+ Thread.currentThread().getStackTrace()[3].toString());
	}

	public void androidClick(By element) throws Exception {
		try {

			wait.until(ExpectedConditions.elementToBeClickable(element)).click();
			logPass("Objeto <b>" + element + "</b> clicado com sucesso.");
			logScreenshot();
		} catch (TimeoutException e) {
			logFail("Erro no método androidClick - O objeto \"<b>" + element
					+ "</b>\" não foi encontrado ou não estava clicável na aplicação.");
			logScreenshot();

			logFaultyCodeLines();
			throw e;
		} catch (Exception e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Erro no método androidClick - Erro não mapeado ao tentar clicar no elemento \"<b>" + element
							+ "</b>\".");
			logFaultyCodeLines();
			e.printStackTrace();
			throw e;
		}
	}

	public void androidTypeInto(By element, String text) throws Exception {
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(element)).sendKeys(text);
			logScreenshot();
			logPass("Texto \"" + text + "\" digitado no objeto \"" + element + "\" com sucesso.");
		} catch (TimeoutException e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Erro no método androidTypeInto - O objeto \"<b>"
					+ element + "</b>\" não foi encontrado na aplicação.");
			logFaultyCodeLines();
			throw e;
		} catch (Exception e) {
			ExtentTestManager.getTest().log(LogStatus.FAIL,
					"Erro no método androidTypeInto - Não foi possível digitar o texto \"" + text
							+ "\" no objeto \"<b>" + element + "</b>\" fornecido.");
			logFaultyCodeLines();
			throw e;
		}
	}

	public void androidDragAndDrop(By draggedElement, By destinationElement) throws Exception {
		try {
			WebElement source = wait.until(ExpectedConditions.presenceOfElementLocated(draggedElement));
			WebElement destination = wait.until(ExpectedConditions.presenceOfElementLocated(destinationElement));
			touch.longPress(longPressOptions().withElement(element(source))).moveTo(element(destination)).release().perform();
			logPass("Elemento <b>" + draggedElement + "</b> arrastado até o elemento <b>" + destinationElement + "</b> com sucesso.");
		} catch (Exception e) {
			logFail("Não foi possível arrastar o elemento <b>" + draggedElement + "</b> até o destino.");
			logFaultyCodeLines();
			throw e;
		}
	}

	public void androidTap(By element) throws Exception {
		try {
			WebElement elementTap = wait.until(ExpectedConditions.elementToBeClickable(element));
			touch.tap(tapOptions().withElement(element(elementTap))).perform();
		} catch (Exception e) {
			logFail("Erro no método androidTap - Não foi possível clicar no elemento <b>" + element + "</b>.");
			logFaultyCodeLines();
			throw e;
		}
	}

	public void androidLongPress(By element, int seconds) throws Exception {
		try {
			WebElement elementPress = wait.until(ExpectedConditions.elementToBeClickable(element));
			touch.longPress(longPressOptions().withElement(element(elementPress)).withDuration(ofSeconds(seconds))).release().perform();
			logPass("O objeto <b> " + element + "</b> foi pressionado por " + Integer.toString(seconds) + " com sucesso.");
		} catch (Exception e) {
			logFail("Não foi possível pressionar o elemento <b>" + element + ".");
			logFaultyCodeLines();
			throw e;
		}
	}

	public void checkIfObjectExists(By by, int timeout) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(by));
			logPass("O objeto <b>" + by.toString() + "</b> foi encontrado na aplicação.");
		} catch (Exception e) {
			logFail("O objeto <b>" + by.toString() + "</b> NÃO foi encontrado na aplicação.");
			logFaultyCodeLines();
			throw e;
		}
	}

	public void checkIfObjectDoesNotExist(By by, int timeout) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		try {
			wait.until(ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(by)));
			logPass("O objeto <b>" + by.toString() + "</b> não foi encontrado na aplicação, como esperado.");
		} catch (Exception e) {
			logFail("O elemento <b>" + by.toString() + "</b> foi encontrado na aplicação, o que <b>NÃO</b> era esperado.");
			logFaultyCodeLines();
			throw e;
		}
	}

	public boolean checkIfObjectExistsOptional(By by, int timeout) throws Exception {
		List<MobileElement> foundObjects = new ArrayList<MobileElement>();
		boolean exist = false;
		try {
			foundObjects = driver.findElements(by);
			int counter = timeout;
			while (counter > -1) {
				if (foundObjects.size() == 0 && counter == 0) {
					logInfo("O objeto <b>" + by + "</b> não foi encontrado na aplicação.");
					ExtentTestManager.getTest().log(LogStatus.INFO, ExtentTestManager.getTest().addBase64ScreenShot(
							"data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64)));
					break;
				} else if (foundObjects.size() > 0) {
					logInfo("O objeto <b>" + by + "</b> foi encontrado na aplicação.");
					exist = true;
					break;
				} else {
					Thread.sleep(1000);
					foundObjects = driver.findElements(by);
					counter--;
				}
			}

		} catch (Exception e) {
			logFail("Erro não mapeado no método checkIfObjectExistsOptional.");
			logFaultyCodeLines();
			throw e;
		}
		return exist;
	}

	public void checkIfEnabled(By by) throws Exception {
		try {
			boolean isEnabled = driver.findElement(by).isEnabled();
			if (isEnabled == true) {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"O objeto <b>" + by.toString() + "</b> está habilitado, como esperado.");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "O objeto <b>" + by.toString() + "</b> não está habilitado.");
				ExtentTestManager.getTest().log(LogStatus.INFO, ExtentTestManager.getTest().addBase64ScreenShot(
						"data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64)));
				logFaultyCodeLines();
			}
		} catch (Exception e) {
			logFail("Erro não mapeado no método checkIfEnabled.");
			logFaultyCodeLines();
			throw e;
		}
	}

	public void checkIfDisabled(By by) throws Exception {
		try {
			boolean isEnabled = driver.findElement(by).isEnabled();
			if (isEnabled == false) {
				ExtentTestManager.getTest().log(LogStatus.PASS,
						"O objeto <b>" + by.toString() + "</b> não está habilitado, como esperado.");
			} else {
				ExtentTestManager.getTest().log(LogStatus.FAIL, "O objeto <b>" + by.toString() + "</b> está habilitado.");
				ExtentTestManager.getTest().log(LogStatus.INFO, ExtentTestManager.getTest().addBase64ScreenShot(
						"data:image/png;base64," + ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64)));
			}
		} catch (Exception e) {
			logFail("Erro não mapeado no método checkIfDisabled.");
			logFaultyCodeLines();
			throw e;
		}
	}

	public String getTextAndroid(By object) throws Exception {
		//String text = null;
		try {
			String text = wait.until(ExpectedConditions.presenceOfElementLocated(object)).getAttribute("text");
			logInfo("O texto exibido no objeto <b>" + object + "</b> é <b>" + text + "</b>.");
			return text;
		} catch (Exception e) {
			logFail("Erro não mapeado no método getTextAndroid");
			logFaultyCodeLines();
			throw e;
		}
	}

	public void assertStrings(String expected, String actual) throws Exception {
		try {
			Assert.assertEquals(expected, actual);
			ExtentTestManager.getTest().log(LogStatus.PASS,
					"As duas strings fornecidas são iguais. O esperado era " + expected + ".");
		} catch (Exception e) {
			logFail("As duas Strings fornecidas não são iguais. O esperado era" + expected + " mas a string recebida foi "
					+ actual + ".");
			logFaultyCodeLines();
			throw e;
		}
	}

	public void assertSubstring(String expectedSubstring, String actualString) {
		try {
			Assert.assertTrue(actualString.contains(expectedSubstring));
			logPass("A segunda String fornecida é uma substring da primeira.");
		} catch (Exception e) {
			logFail("Erro no método assertSubstring - A segunda String fornecida não é uma substring da primeira.");
			logFaultyCodeLines();
			throw e;
		}
	}

	public void checkCountObjects(By by, int maximumElements) throws Exception {
		try {
			wait.until(ExpectedConditions.numberOfElementsToBeLessThan(by, maximumElements + 1));
			logPass("O numéro de objetos <b>" + by + "</b> encontrados na aplicação é menor ou igual ao número máximo esperado.");
		} catch (Exception e) {
			logFail("O numero de objetos <b>" + by + "</b> encontrados na aplicação é maior do que o número máximo esperado.");
			logFaultyCodeLines();
			throw e;
		}
	}

	public List<MobileElement> findObjects(By by, int timeout) throws Exception {
		List<MobileElement> foundObjects = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		try {
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
			foundObjects = driver.findElements(by);
		} catch (Exception e) {
			logFail("Erro não mapeado no método findObjects.");
			logFaultyCodeLines();
			throw e;
		}
		return foundObjects;

	}

	public void espera(int tempo) {
		try {
			Thread.sleep(tempo);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void scroll(double start, double end) {

		Dimension dimension = driver.manage().window().getSize();
		int scrollStart = (int) (dimension.getHeight() * start);
		int scrollEnd = (int) (dimension.getHeight() * end);

		new TouchAction((PerformsTouchActions) driver)
				.press(point(0, scrollStart))
				.waitAction(waitOptions(Duration.ofSeconds(1)))
				.moveTo(point(0, scrollEnd))
				.release().perform();
	}

	public void validateEqualText(By by, String textExpected) {
		String text = driver.findElement(by).getText();
		if (!text.equals(textExpected)) ;
		Assert.fail("texto difere do esperado");
	}

	public void click(By element) {
		driver.findElement(element).click();
	}

	public void swipeHorizontal(double v, double v1, double anchorPercentage) {
		Dimension dimension = driver.manage().window().getSize();
		int anchor = (int) (dimension.height * anchorPercentage);
		int startPoint = (int) (dimension.width * v);
		int endPoint = (int) (dimension.width * v1);

		new TouchAction((PerformsTouchActions) driver)
				.press(point(startPoint, anchor))
				.waitAction(waitOptions(ofMillis(4)))
				.moveTo(point(endPoint, anchor))
				.release().perform();
	}
	//Press by coordinates
	public void pressByCoordinates (int x, int y, long seconds) {
		new TouchAction(driver)
				.press(point(x,y))
				.waitAction(waitOptions(ofSeconds(seconds)))
				.release()
				.perform();
	}
	//Tap by coordinates
	public void tapByCoordinates (int x, int y) {
		new TouchAction(driver)
				.tap(point(x,y))
				.waitAction(waitOptions(ofMillis(250))).perform();
	}

}










