package reports;

import java.io.File;

import com.relevantcodes.extentreports.ExtentReports;

import common.CommonUtils;
import common.ProjectConstants;

//OB: ExtentReports extent instance created here. That instance can be reachable by getReporter() method.
public class ExtentManager {

	private static ExtentReports extent;

	public synchronized static ExtentReports getReporter() {
		String extentConfigHost = CommonUtils.getProperty("EXTENT_CONFIG_HOST", ProjectConstants.getFrameworkConfigsFolder() + "ProjectConfigs.properties");
		String ambiente = CommonUtils.getProperty("EXTENT_CONFIG_AMBIENTE", ProjectConstants.getFrameworkConfigsFolder() + "ProjectConfigs.properties");
		String username = System.getProperty("user.name");
		if (extent == null) {
			// Set HTML reporting file location
			String workingDir = System.getProperty("user.dir");
			if (System.getProperty("os.name").toLowerCase().contains("win")) {
				extent = new ExtentReports(workingDir + "\\reports\\" + CommonUtils.dataAgora() + ".html", true);
				extent.addSystemInfo("Host Name", extentConfigHost)
						.addSystemInfo("Environment", ambiente)
						.addSystemInfo("User Name", username);
				extent.loadConfig(new File(workingDir +  "\\extent-config.xml"));
				System.out.println("Report on");
			} else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
				extent = new ExtentReports(
						workingDir + "\\reports\\" + CommonUtils.dataAgora() + ".html", true);
				extent.addSystemInfo("Host Name",
						CommonUtils.getProperty("EXTENT_CONFIG_HOST",
								ProjectConstants.getFrameworkConfigsFolder() + "ProjectConfigs.properties"))
						.addSystemInfo("Environment",
								CommonUtils.getProperty("EXTENT_CONFIG_AMBIENTE",
										ProjectConstants.getFrameworkConfigsFolder() + "ProjectConfigs.properties"))
						.addSystemInfo("User Name", System.getProperty("user.name"));
				extent.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
			} else {
				extent = new ExtentReports(
						System.getProperty("user.dir") + "\\reports\\" + CommonUtils.dataAgora() + ".html", true);
				System.out.println("Created report file: " + System.getProperty("user.dir") + "/output-files/" + CommonUtils.dataAgora() + ".html");
				extent.addSystemInfo("Host Name",
						CommonUtils.getProperty("EXTENT_CONFIG_HOST",
								ProjectConstants.getFrameworkConfigsFolder() + "ProjectConfigs.properties"))
						.addSystemInfo("Environment",
								CommonUtils.getProperty("EXTENT_CONFIG_AMBIENTE",
										ProjectConstants.getFrameworkConfigsFolder() + "ProjectConfigs.properties"))
						.addSystemInfo("User Name", System.getProperty("user.name"));
				extent.loadConfig(new File(System.getProperty("user.dir") + "//extent-config.xml"));
				System.out.println("Report config loaded: " + System.getProperty("user.dir") + "/output-files/" + CommonUtils.dataAgora() + ".html");
			}
		}
		return extent;
	}
}