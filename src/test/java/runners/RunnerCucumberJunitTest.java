package runners;

import static configuration.TestContext.getInstance;
import static configuration.TestContext.init;
import static reports.ExtentManager.getReporter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		tags="@Done02",
		strict=true,
		features="classpath:features/",
		glue="stepdefs",
		plugin={"html:target/cucumber-html-report","json:target/cucumber.json"}
		)
public class RunnerCucumberJunitTest {
	
	@BeforeClass
	public static void setUp() {
		init();
	}
	
	@AfterClass
	public static void tearDown() {
		getReporter().flush();
		getInstance().destroy();
	}

}
