package configuration;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.remote.AndroidMobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import common.ProjectConstants;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class AppiumDriverManager {
	private static JsonManager jsonManager = new JsonManager();
	private static String configJson = "executionParameters.json";
	private static String keyTipoExecucao = "tipoExecucao";
	private static String keyNomeApk = "nomeApk";
	private static String keyUrlAppium = "urlAppium";
	private static String keyDeviceName = "deviceName";
	private static int appiumImplicitlyWait = 5;
	
	public static AppiumDriver<MobileElement> configureDriver() {
		AppiumDriver<MobileElement> driver = null;
		try {
//			HashMap<String, String> parametrosExecucao = jsonManager.readJson(ProjectConstants.getFrameworkConfigsFolder()+configJson).get(0);
			String tipoExecucao = "local";//parametrosExecucao.get(keyTipoExecucao);
			String nomeApk = "App-natura-universal-hml.apk";//parametrosExecucao.get(keyNomeApk);
			String urlAppium = "http://localhost:4723/wd/hub/";// parametrosExecucao.get(keyUrlAppium);
			String deviceName = "Android9";//parametrosExecucao.get(keyDeviceName);
			switch (tipoExecucao.trim().toLowerCase()) {
			case "local":
				driver = getAndroidLocalDriver(urlAppium, appiumImplicitlyWait, nomeApk, deviceName);
				break;
			default:
				driver = getAndroidLocalDriver(urlAppium, appiumImplicitlyWait, nomeApk, deviceName);
				break;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return driver;
	}
	
	private static AppiumDriver<MobileElement> getAndroidLocalDriver(String appiumServerUrl, int implicitlyWait, String apkName,
			String deviceName) {
		URL serverAppiumUrl = null;
		System.out.println(appiumServerUrl);
		System.out.println(apkName);
		System.out.println(deviceName);
		System.out.println(implicitlyWait);
		try {
			serverAppiumUrl = new URL(appiumServerUrl);
		} catch(Exception e) {
			e.printStackTrace();
		}
		

		File apkFolder = new File("src/test/resources/apk");
		File apk = new File(apkFolder, apkName);

		DesiredCapabilities capabilities = new DesiredCapabilities();

		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UIAutomator2");
		//capabilities.setCapability(MobileCapabilityType.APP, apk.getAbsolutePath());
		capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE,"net.natura.cn.hml");
		capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,"net.natura.cn.especialista.ui.SplashActivity");
		AppiumDriver<MobileElement> androidDriver = new AppiumDriver<MobileElement>(serverAppiumUrl, capabilities);
		androidDriver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
		return androidDriver;
	}
}
